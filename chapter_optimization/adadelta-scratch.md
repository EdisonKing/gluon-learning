# Adadelta --- 从0开始


我们在[Adagrad](adagrad-scratch.md)里提到，由于学习率分母上的变量$\mathbf{s}$一直在累加按元素平方的梯度，每个元素的学习率在迭代过程中一直在降低或不变。所以在有些问题下，当学习率在迭代早期降得较快时且当前解依然不理想时，Adagrad在迭代后期可能较难找到一个有用的解。我们在[RMSProp](rmsprop-scratch.md)介绍了应对这一问题的一种方法：对梯度按元素平方使用指数加权移动平均而不是累加。

事实上，Adadelta也是一种应对这个问题的方法。有意思的是，它没有学习率参数。


## Adadelta算法

**Adadelta算法也像RMSProp一样，使用了一个梯度按元素平方的指数加权移动平均变量$\mathbf{s}$，并将其中每个元素初始化为0。在每次迭代中，首先计算[小批量梯度](gd-sgd-scratch.md) $\mathbf{g}$，然后对该梯度按元素平方后做指数加权移动平均并计算$\mathbf{s}$：**

$$\mathbf{s} := \rho \mathbf{s} + (1 - \rho) \mathbf{g} \odot \mathbf{g} $$

然后我们计算当前需要更新的参数的变化量：

$$ \mathbf{g}^\prime = \frac{\sqrt{\Delta\mathbf{x} + \epsilon}}{\sqrt{\mathbf{s} + \epsilon}}   \odot \mathbf{g} $$


**其中$\epsilon$是为了维持数值稳定性而添加的常数，例如$10^{-5}$。和Adagrad一样，模型参数中每个元素都分别拥有自己的学习率。其中$\Delta\mathbf{x}$初始化为零张量，并做如下$\mathbf{g}^\prime$按元素平方的指数加权移动平均：**

$$\Delta\mathbf{x} := \rho \Delta\mathbf{x} + (1 - \rho) \mathbf{g}^\prime \odot \mathbf{g}^\prime $$

同样地，最后的参数迭代步骤与小批量随机梯度下降类似。只是这里梯度前的学习率已经被调整过了：

$$\mathbf{x} := \mathbf{x} - \mathbf{g}^\prime $$


## Adadelta的实现

Adadelta的实现很简单。我们只需要把上面的数学公式翻译成代码。

```{.python .input  n=1}
# Adadalta
def adadelta(params, sqrs, deltas, rho, batch_size):
    eps_stable = 1e-5
    for param, sqr, delta in zip(params, sqrs, deltas):
        g = param.grad / batch_size
        sqr[:] = rho * sqr + (1. - rho) * nd.square(g)
        cur_delta = nd.sqrt(delta + eps_stable) / nd.sqrt(sqr + eps_stable) * g
        delta[:] = rho * delta + (1. - rho) * cur_delta * cur_delta
        param[:] -= cur_delta 
```

## 实验

实验中，我们以线性回归为例。其中真实参数`w`为[2, -3.4]，`b`为4.2。我们把算法中基于指数加权移动平均的变量初始化为和参数形状相同的零张量。

```{.python .input  n=2}
from mxnet import ndarray as nd
import mxnet as mx
from mxnet import autograd
from mxnet import gluon
import random

mx.random.seed(1)
random.seed(1)

# 生成数据集。
num_inputs = 2
num_examples = 1000
true_w = [2, -3.4]
true_b = 4.2
X = nd.random_normal(scale=1, shape=(num_examples, num_inputs))
y = true_w[0] * X[:, 0] + true_w[1] * X[:, 1] + true_b
y += .01 * nd.random_normal(scale=1, shape=y.shape)
dataset = gluon.data.ArrayDataset(X, y)

# 构造迭代器。
import random
def data_iter(batch_size):
    idx = list(range(num_examples))
    random.shuffle(idx)
    for batch_i, i in enumerate(range(0, num_examples, batch_size)):
        j = nd.array(idx[i: min(i + batch_size, num_examples)])
        yield batch_i, X.take(j), y.take(j)

# 初始化模型参数。
def init_params():
    w = nd.random_normal(scale=1, shape=(num_inputs, 1))
    b = nd.zeros(shape=(1,))
    params = [w, b]
    sqrs = []
    deltas = []
    for param in params:
        param.attach_grad()
        # 把算法中基于指数加权移动平均的变量初始化为和参数形状相同的零张量。
        sqrs.append(param.zeros_like())
        deltas.append(param.zeros_like())
    return params, sqrs, deltas

# 线性回归模型。
def net(X, w, b):
    return nd.dot(X, w) + b

# 损失函数。
def square_loss(yhat, y):
    return (yhat - y.reshape(yhat.shape)) ** 2 / 2
```

接下来定义训练函数。当epoch大于2时（epoch从1开始计数），学习率以自乘0.1的方式自我衰减。训练函数的period参数说明，每次采样过该数目的数据点后，记录当前目标函数值用于作图。例如，当period和batch_size都为10时，每次迭代后均会记录目标函数值。

```{.python .input  n=3}
%matplotlib inline
import matplotlib as mpl
mpl.rcParams['figure.dpi']= 120
import matplotlib.pyplot as plt
import numpy as np

def train(batch_size, rho, epochs, period):
    assert period >= batch_size and period % batch_size == 0
    [w, b], sqrs, deltas = init_params()
    total_loss = [np.mean(square_loss(net(X, w, b), y).asnumpy())]

    # 注意epoch从1开始计数。
    for epoch in range(1, epochs + 1):
        for batch_i, data, label in data_iter(batch_size):
            with autograd.record():
                output = net(data, w, b)
                loss = square_loss(output, label)
            loss.backward()
            adadelta([w, b], sqrs, deltas, rho, batch_size)
            if batch_i * batch_size % period == 0:
                total_loss.append(np.mean(square_loss(net(X, w, b), y).asnumpy()))
        print("Batch size %d, Epoch %d, loss %.4e" % 
              (batch_size, epoch, total_loss[-1]))
    print('w:', np.reshape(w.asnumpy(), (1, -1)), 
          'b:', b.asnumpy()[0], '\n')
    x_axis = np.linspace(0, epochs, len(total_loss), endpoint=True)
    plt.semilogy(x_axis, total_loss)
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.show()
```

使用Adadelta，最终学到的参数值与真实值较接近。

```{.python .input  n=4}
train(batch_size=10, rho=0.9999, epochs=3, period=10)
```

```{.json .output n=4}
[
 {
  "name": "stdout",
  "output_type": "stream",
  "text": "Batch size 10, Epoch 1, loss 5.3437e-05\nBatch size 10, Epoch 2, loss 4.8807e-05\nBatch size 10, Epoch 3, loss 4.9028e-05\nw: [[ 2.001109  -3.4000478]] b: 4.2014136 \n\n"
 },
 {
  "data": {
   "image/png": "iVBORw0KGgoAAAANSUhEUgAAAY4AAAEKCAYAAAAFJbKyAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMi4yLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvhp/UCwAAIABJREFUeJzt3XmYXHWd7/H3t7be0t3ZOlt3VhISEpYkhAgyqAgyQYEAMpK4IwqOFx2fmXsV7zjjM/e5zvLMjPcOV0ZEYUBlAgyKEzFOREQiytasSQgJTUKSzkI6Wyfpvaq+94+qxKbJUt1d1afq9Of1PPWk6/TpU99fn0596nd+5/yOuTsiIiK5igRdgIiIlBYFh4iI9IuCQ0RE+kXBISIi/aLgEBGRflFwiIhIvyg4RESkXxQcIiLSLwoOERHpl1jQBRTC2LFjfdq0aUGXISJSUp5//vm97l53qvVCGRzTpk2jsbEx6DJEREqKmW3NZT0dqhIRkX4JVXCY2ZVmdmdra2vQpYiIhFaogsPdf+buN9XW1gZdiohIaIUqOEREpPAUHCIi0i8KDhER6RcFh4iI9IuCo5dfvfoW31uzOegyRESKWtEHh5nNMLO7zOyhQr/WE5ta+Najm+jsSRX6pURESlYgwWFmd5vZHjNb12f5EjPbaGZNZnYrgLtvdvcbh6KuD8wdT0dPiidf3zsULyciUpKC6nHcAyzpvcDMosDtwOXAXGC5mc0dyqLOnzGGEWUxHn31raF8WRGRkhJIcLj7GmB/n8WLgaZsD6MbuB9YOpR1JWIR3je7jtWv7qatKzmULy0iUjKKaYyjHtje63kzUG9mY8zsDmCBmX3tRD9sZjeZWaOZNba0tAy4iBsunM7B9h5+9HROc32JiAw7RT87rrvvAz6fw3p3mtku4MpEInHuQF/v3KmjuGjWWL67ZjPL3zWFmvL4QDclIhJKxdTj2AFM7vW8IbssZ/maq+qrS+ZwoL2b2371+qC2IyISRsUUHM8Bs8xsupklgGXAyv5sIF+z455ZX8u1Cxr44dNbOaKxDhGRtwnqdNwVwFPAbDNrNrMb3T0J3AKsBjYAD7r7+v5sN5+z4y5bPJmuZJrHNugMKxGR3gIZ43D35SdYvgpYNdDtmtmVwJUzZ84c6CaOOXfKKCbUlPPIK7tYOr9+0NsTEQmLYjpUNWj57HFEIsaSMyfwxKYWOrp1JbmIyFGhCo58e/+ccXQn0zy9eV/QpYiIFI1QBUe+bx27ePpoKuJRfrNxT162JyISBqEKjnzfOrY8HuWC08bw+MYW3D0v2xQRKXWhCo5CWDJvAtv2t9O49UDQpYiIFIVQBUe+D1UBXHHOREaUxVjxzLa8bVNEpJSFKjjyfagKoDIR4+oFk3hk7S4OtnfnbbsiIqUqVMFRKMsXT6E7mebhF/s1A4qISCiFKjgKcagKYN6kWs5pqGXFs9s0SC4iw16ogqMQh6qO+sh5k9n01hHW7zyU922LiJSSUAVHIX3wzInEo8bKl3cGXYqISKAUHDkaVZXgvafXsfKlnaTSOlwlIsOXgqMfrlnQwO5Dnfz29YHfYVBEpNSFKjgKNTh+1KVzxzG6KsEDz20/9coiIiEVquAo5OA4QFksyrUL6nn01bfYe6SrIK8hIlLsQhUcQ+H68yaTTDs/eaE56FJERAKh4OinWeOrOXfqKO5/bruu6RCRYUnBMQAfP38Km1va+NUGTbcuIsOPgmMArjx7ElNGV3LbY6+r1yEiw06ogqPQZ1UdFYtG+NIls1i7o5WHntdYh4gML6EKjkKfVdXbtQvqWTR1FH+7agOtHT0Ffz0RkWIRquAYSpGI8TdL53GgvYc7nngj6HJERIaMgmMQ5k2q5er5k7j7yS3sbu0MuhwRkSGh4Bikv7hsNml3/u+vNgVdiojIkFBwDNLk0ZV8/PypPNi4naY9R4IuR0Sk4BQceXDLxTOpTMT4x9WvBV2KiEjBKTjyYMyIMj530QxWr3+LTW8dDrocEZGCKvrgMLMqM7vXzL5nZh8Lup4T+cQFU0lEI6x4dlvQpYiIFFQgwWFmd5vZHjNb12f5EjPbaGZNZnZrdvG1wEPu/jngqiEvNkejqxJcNm88P3lhBx3dqaDLEREpmKB6HPcAS3ovMLMocDtwOTAXWG5mc4EG4OgNMIr6HfkT50+ltaOHh57X/TpEJLwCCQ53XwPs77N4MdDk7pvdvRu4H1gKNJMJDzhJvWZ2k5k1mlljS0swd+hbPH00C6eM5LtrNpNMpQOpQUSk0IppjKOeP/QsIBMY9cBPgA+b2XeAn53oh939Tndf5O6L6urqClvpCZgZN7/3NJoPdPDr1zRzroiEUyzoAk7F3duAG3JZ18yuBK6cOXNmYYs6iUvmjGN8TRkrnt3GZfMmBFaHiEihFFOPYwcwudfzhuyynA3lJIcnEotGuH7RZH6zqUUXBIpIKBVTcDwHzDKz6WaWAJYBK/uzgaGaVv1UPvnuaVSXxfjGynW6X4eIhE5Qp+OuAJ4CZptZs5nd6O5J4BZgNbABeNDd1/dnu8XQ4wAYO6KMP//A6fyuaR8vbT8YaC0iIvkW1FlVy919orvH3b3B3e/KLl/l7qe7+2nu/s3+brdYehwAVy+oJ2JokFxEQqeYDlUNWrH0OABGViZYNHW0gkNEQidUwVFMPQ6Ai+eMY/3OQ+xq7Qi6FBGRvAlVcBRTjwNgyZmZ03F//squgCsREcmfUAVHsZk+toqz6mtZ+fLOoEsREcmbUAVHsR2qArjqnEm80tzKa7sPBV2KiEhehCo4iu1QFcB15zZQXRbjn3+pW8uKSDiEKjiK0aiqBDe/dwaPvvoWz2/tO6+jiEjpUXAMgRsunM7YEWX8wy826kpyESl5oQqOYhzjAKgqi/HF98/k2Tf388K2A0GXIyIyKKEKjmIc4zjqmoX1xKPGL9e/FXQpIiKDEqrgKGY15XHOnzGGR19VcIhIaVNwDKEPzB3P5r1tNO05HHQpIiIDFqrgKNYxjqOWzJtAxODhF/t1mxERkaISquAo5jEOgHE15Vw0q46HX9hBOq2zq0SkNIUqOErBtQvr2dnayW+b9gZdiojIgCg4htiSMycwrrqM763ZHHQpIiIDouAYYmWxKJ++cBpPNu1lwy7NXyUipUfBEYDl500hHjUeer456FJERPpNwRGAUVUJ3j9nHP/50k6SqXTQ5YiI9EuogqPYT8ft7ZoFDew90qVBchEpOaEKjmI/Hbe3i+fUMbIyzsMv6JoOESktoQqOUlIWi3LF2RNZvX43hzt7gi5HRCRnCo4AfXhhA13JtK4kF5GSouAI0PzJI5k/eSR3PbmFlK4kF5ESoeAIkJlx83tmsHVfO79cvzvockREcqLgCNhl8yYwZXQl312zWXcHFJGSUPTBYWYzzOwuM3so6FoKIRoxPnvRdF7aflB3BxSRklDQ4DCzu81sj5mt67N8iZltNLMmM7v1ZNtw983ufmMh6wzadec2MKIsxn3PbAu6FBGRUyp0j+MeYEnvBWYWBW4HLgfmAsvNbK6ZnWVmj/R5jCtwfUWhMhHjqvmTWLV2F60dOjVXRIpbQYPD3dcA+/ssXgw0ZXsS3cD9wFJ3X+vuV/R57ClkfcVk+XlT6OxJ858v6dRcESluQYxx1APbez1vzi47LjMbY2Z3AAvM7GsnWe8mM2s0s8aWlpb8VTtEzmqoZd6kGlY8u12D5CJS1Ip+cNzd97n75939NHf/u5Osd6e7L3L3RXV1dUNZYt4sWzyFDbsO8Upz8c+1JSLDVxDBsQOY3Ot5Q3bZoJXSJIfHs3T+JEaUxfj+k1uCLkVE5ISCCI7ngFlmNt3MEsAyYGUAdRSdmvI4H3vXFH7+yk627WsPuhwRkeMq9Om4K4CngNlm1mxmN7p7ErgFWA1sAB509/X5eL1Smh33RD7zR9OJRSJ877e6tayIFKdYITfu7stPsHwVsKqQr12qxteUc82Ceh5s3M6fXTqLsSPKgi5JRORtin5wvD9KfYzjqJveO4PuVJof/P7NoEsREXmHUAVHGA5VAZxWN4JL5oznh09vpaM7FXQ5IiJvE6rgCEuPA+BzF03nQHsP9z2zNehSRETeJlTBEZYeB8Di6aN5z+l1/Mtjr7P3SFfQ5YiIHBOq4AgTM+Ovr5hLR3eKf1q9MehyRESOCVVwhOlQFcDMcSO44cJpPNC4nVeaDwZdjogIELLgCNOhqqO+eEnmlNy/fHgdyVQ66HJERMIVHGFUUx7nG1fOZe2OVh56vjnockREwhUcYTtUddSHzprI6eNH8EDj9lOvLCJSYDkFh5n9mZnVWMZdZvaCmV1W6OL6K4yHqiAzUP7hhQ28uO0gm1uOBF2OiAxzufY4PuPuh4DLgFHAJ4C/L1hV8g5XL6gnYvCTF3SjJxEJVq7BYdl/Pwj8MDspoZ1kfcmz8TXl/NGsOh5+cQfptG70JCLByTU4njezX5IJjtVmVg3oFJ8h9uGF9ew42MHTm/cFXYqIDGO5BseNwK3Aee7eDsSBGwpW1QCFdXD8qD+eN4Hqshg/1uEqEQlQrsFxAbDR3Q+a2ceBrwNF9+4c1sHxo8rjUT509kR+sW4X7d3JoMsRkWEq1+D4DtBuZucAfwG8AfygYFXJCX343Abau1M88squoEsRkWEq1+BIursDS4Fvu/vtQHXhypITWTR1FHMn1vAvv3qdzh5NuS4iQy/X4DhsZl8jcxruz80sQmacQ4aYmfH1D53BjoMd3P27LUGXIyLDUK7BcT3QReZ6jt1AA/CPBatKTurdM8dy6Rnj+dfH36DlsKZcF5GhlVNwZMPiPqDWzK4AOt296MY4wn5WVW9f++AcOnpSfP/JzUGXIiLDTK5TjnwEeBb4E+AjwDNmdl0hCxuIsJ9V1dtpdSO4ePY4Hn5hh2bNFZEhleuhqr8kcw3Hp9z9k8Bi4K8KV5bk4sML69lzuIvfvaELAkVk6OQaHBF339Pr+b5+/KwUyPvPGEdtRZwfa7p1ERlCsRzX+y8zWw2syD6/HlhVmJIkV2WxKFedM4kHG7dzuLOH6nKd6CYihZfr4Pj/AO4Ezs4+7nT3rxayMMnNtQvr6Uqm+bkuCBSRIZJrjwN3/zHw4wLWIgMwf/JI5kyo5vtPbuEjiyYTiWjSYhEprJP2OMzssJkdOs7jsJkdGqoi5cTMjC9cPJOmPUdYvX530OWIyDBw0uBw92p3rznOo9rda4aqSDO72sy+Z2YPFOOdB4P2obMmMqm2XLPmisiQKPiZUWZ2t5ntMbN1fZYvMbONZtZkZreebBvu/lN3/xzweTID89JLNGJ8YO54nmxq0fxVIlJwQ3FK7T3Akt4LzCwK3A5cDswFlpvZXDM7y8we6fMY1+tHv579OenjkjPG09mT5ndNe4MuRURCLufB8YFy9zVmNq3P4sVAk7tvBjCz+4Gl7v53wBV9t2FmRuYe579w9xeO9zpmdhNwE8CUKVPyVn+peNeM0VSXxXj4xR1ccsb4oMsRkRAL6iK+emB7r+fN2WUn8kXgUuA6M/v88VZw9zvdfZG7L6qrq8tfpSWiLBblo+dPYdXaXWzZ2xZ0OSISYiVx9be73+bu57r75939jhOtN5wmOTyez/7RDOLRCHeu0cSHIlI4QQXHDmByr+cN2WWDMpwmOTyeuuoyls6fxE9f3EFrR0/Q5YhISAUVHM8Bs8xsupklgGXAysFudLj3OAA+ecE0OnpSmr9KRApmKE7HXQE8Bcw2s2Yzu9Hdk8AtwGpgA/Cgu68f7GsN9x4HwJn1tcyfPJIfPb2VdNqDLkdEQqjgweHuy919orvH3b3B3e/KLl/l7qe7+2nu/s18vJZ6HBmfvGAqm/e28XtNty4iBVASg+O5Uo8j44NnTWR0VYIfPPVm0KWISAiFKjjU48goj0e5/rzJ/GrDW+w42BF0OSISMqEKDvU4/uCji6fgwL8/szXoUkQkZEIVHPIHk0dXcsmccTzw3Ha6kpq/SkTyJ1TBoUNVb/eJC6ax90g3/7VO062LSP6EKjh0qOrtLpo5lqljKrn/2e2nXllEJEehCg55u0jEuHp+PU9v2cfu1s6gyxGRkFBwhNzS+ZNwh5+9vDPoUkQkJEIVHBrjeKcZdSOYP3kkK57dpivJRSQvQhUcGuM4vhsunMbmvW38ZtOeoEsRkRAIVXDI8X3wrIlMrC3n+7/dEnQpIhICCo5hIB6N8Ol3T+P3b+xj/U4dxhORwQlVcGiM48SWLZ5CZSLKXU+q1yEigxOq4NAYx4nVVsT5yKLJ/Ozlnbx1SKfmisjAhSo45OQ+c+F0kmnXrLkiMigKjmFkyphK/njuBO57Zhvt3cmgyxGREqXgGGZuvGg6B9t7+PELg77Fu4gMUwqOYWbR1FGc01DL3U9u0QWBIjIgCo5hxsy48aIZbNnbxq9f0wWBItJ/oQoOnY6bm8vPnMCk2nK+/+TmoEsRkRIUquDQ6bi5iUcjfPrCaTy9eT/rdihkRaR/QhUckrvrz5tClS4IFJEBUHAMU7UVcT5yXuaCQN2rQ0T6Q8ExjN3w7umk3bnn928GXYqIlBAFxzA2ZUwlS86cwH1Pb6W1oyfockSkRCg4hrkvvG8mh7uS/PCpN4MuRURKRNEHh5mdYWZ3mNlDZvanQdcTNmfW13Lx7DruenKLpiERkZwUNDjM7G4z22Nm6/osX2JmG82sycxuPdk23H2Du38e+AhwYSHrHa5uef8sDrT38O/PbAu6FBEpAYXucdwDLOm9wMyiwO3A5cBcYLmZzTWzs8zskT6PcdmfuQr4ObCqwPUOS+dOHcW7TxvDd9dsprMnFXQ5IlLkChoc7r4G2N9n8WKgyd03u3s3cD+w1N3XuvsVfR57sttZ6e6XAx8rZL3D2S3vn0nL4S7+4/nmoEsRkSIXxBhHPbC91/Pm7LLjMrP3mdltZvZdTtLjMLObzKzRzBpbWlryV+0wccGMMSycMpI717xBSpMfishJFP3guLv/xt2/5O43u/vtJ1nvTndf5O6L6urqhrLEUDAzPnvRDLbv7+BxTX4oIicRRHDsACb3et6QXTZomuRwcC6bO56JteXc+9SbQZciIkUsiOB4DphlZtPNLAEsA1YGUIf0EYtG+Pj5U/nt63tp2nM46HJEpEgV+nTcFcBTwGwzazazG909CdwCrAY2AA+6+/p8vJ5mxx28ZedNJhGLcO/vtwZdiogUqVghN+7uy0+wfBU6tbYojRlRxtJzJvHQ8838+QdOZ1RVIuiSRKTIFP3geH9ojCM/bnrPDDp6UhrrEJHjClVw6FBVfswaX82lZ4zj3t+/SUe3LggUkbcLVXCox5E/N7/3NA609/Bg4/ZTrywiw0qogkM9jvw5b9pozp06iu/9djPJVDrockSkiIQqOCS/bn7PDJoPdPDztbuCLkVEikiogkOHqvLr0jPGM3PcCO54YjPumoZERDJCFRw6VJVfkYhx03tmsGHXIda8vjfockSkSIQqOCT/ls6fxPiaMv718aagSxGRIhGq4NChqvwri0W5+T2n8cyW/fz2dc06LCIhCw4dqiqMj50/hYZRFfz9L14jrSnXRYa9UAWHFEZZLMp/v2w263ce4mev7Ay6HBEJmIJDcnLVOZOYN6mGbz26Sb0OkWFOwSE5iUSMm997Glv3tfPU5n1BlyMiAQpVcGhwvLAumzue2oo4DzynaUhEhrNQBYcGxwurPB7l2oX1rFq7i9d2Hwq6HBEJSKiCQwrvS++fRU1FnK/9ZK3msBIZphQc0i+jqhJ848q5vLjtILf9WhcFigxHCg7pt6Xz67lmQT3/+ngT2/e3B12OiAwxBYcMyFeWzCZixneeeCPoUkRkiIUqOHRW1dCZWFvBssWTuf/ZbaxevzvockRkCIUqOHRW1dC69fI5nN0wki+ueJGndW2HyLARquCQoVWZiPFvnz6PKaMruekHjew70hV0SSIyBBQcMiijqhLc8fGFtHWnuO2x14MuR0SGgIJDBm3muGqWnTeZ+57ZxuaWI0GXIyIFpuCQvPjypadTFovwD//1WtCliEiBKTgkL+qqy/j8e09j9fq3eO7N/UGXIyIFpOCQvPnsRTMYX1PG//75BlKael0ktEoiOMysyswazeyKoGuRE6tIRPna5Wfw8vaD/OPqjUGXIyIFUtDgMLO7zWyPma3rs3yJmW00syYzuzWHTX0VeLAwVUo+Xb2gno+9awp3PPEGK1/W3QJFwihW4O3fA3wb+MHRBWYWBW4HPgA0A8+Z2UogCvxdn5//DHAO8CpQXuBaJU++ceU8Nr11mK889DJn19cybWxV0CWJSB4VtMfh7muAviOli4Emd9/s7t3A/cBSd1/r7lf0eewB3gecD3wU+JyZHbdmM7spezirsaWlpXCNklNKxCJ8+6MLiUci/OVP1+pWsyIhE8QYRz3Q+xZyzdllx+Xuf+nuXwb+Hfieux/3JhDufqe7L3L3RXV1dXktWPpvfE05t35wDr9r2sc/P7oRd4WHSFgU+lBV3rj7Padax8yuBK6cOXNm4QuSU/ro4imsbW7l9sff4M297fzTn5xDRSIadFkiMkhB9Dh2AJN7PW/ILhs0TXJYXMyMb15zFl9ZMptV63Zx84+e110DRUIgiOB4DphlZtPNLAEsA1bmY8OaVr34RCPGF943k7+95izWbGrhO7/R/TtESl2hT8ddATwFzDazZjO70d2TwC3AamAD8KC7r8/H66nHUbyWL57CledM4l8ee53bHntdPQ+RElbQMQ53X36C5auAVfl+PY1xFLdvXnMm7s63Ht1E49YD/P21ZzFpZEXQZYlIP1kYz3ZZtGiRNzY2Bl2GnMCKZ7fxjZXrSaWdaWMq+cqSOVw8exyJWElMZCASWmb2vLsvOuV6YQqOXj2Oz73+uu4NUcyaD7TzYGMzq9ftZuNbh0nEInzqgqlcPGcc508fQyRiQZcoMuwMy+A4Sj2O0tGVTPGLtbt5YlMLD7+YObluzoRq5k8eiTs4zrSxVYyuTFAej5JKO5fOHU9tRTzgykXCR8Gh4Cg5+4508cSmFv7td2+y+1AnEYO0Q8vht9+SdkZdFWfX1/Li9oN0J9OUxSKcM3kk8WiEuRNrOGNiDdXlMQ629zB1TCWTR1e+7eeTqTQ9KScaMWIRy6l309aVpKMnRU8qzeHOJGubWznU2cOlZ4xnYm05sWiEZCrNgfYeIpY5m+yoEWUxYtHBHYZLptKkHdbvbOXNfW38vmkf7d0pRlbGGVddTl11GWNHJIhFjfE15UwbU4UZtHenqK2IE4sYu1o7eW33IRrfPEBrRw911WXMqBvBjLFVdPak6E6l2d/WTW1FnAVTRjGiLHbswk0zO3Zr4KY9R0ilnYVTR/HqrkN09qSYPb6aWCRCRSI66EOO7s6hjiSVZVEOdyZp60pSWxmnuixG2mHrvjaiEaNhVCU7DnTwRkumnn1tXZwxsYZ5k2rf9vs/qrWjh1jEaO9OMaoy/o590pNKE7XM30MylWZXayfb97cTj0WYNLKCRDRCe3eSkZUJaivib/vd5FtXMkVrew8HO3roTqaZPKqSqrIou1o7SaWdsniECTXldPSk2Hekm/E15cSjxqGO5KD2wbAMDh2qCqfDnT0c7kzS2ZNi2/52/vo/15NMpTmroZaa8jhHupI8u2U/ZrD3SPc7fj4WMcpiERLZx74j3SR7TYNiBvFI5FiQRKPZfyNGe1eKI91JTvbfJGJQWxGnrTtFd/L4Z4uVxyNUJmJUxKOUxSKk3Uk7pNKOu5PKPk+nnbR7djl4tr7DncljQQowqjLOqKoEB9t72N/2zjb3rS8WidCdPZMtHjWqy+McaO8+YbsilrmnfFu27b1f+2SiEWN0VQL3TN3l8QjxaATPtjWVdpLp9LGvHYiYZR+Zr9u7kxzqTL5j2/GoEY9GaO9OHXut403fP6IsRnV5jIgZZpk6UilnZ2vnsXUqE1FGVSYoi0Xo7ElxqDPJka4kZhz7mzrZrQGqy2N09qQwM6rLYmSyI/t6kP33D4HS3p2kK5mmMhE9TtshEYuSiGbWP9jRc6yNfX+3vWuqSkRp67Xe0X304z+9gHOnjj5h7SczLIPjKPU4hq8397ax82AHhzp7qCmPs/Gtw+w90kVXT5quZJquZIoxI8qorYhn3sRSTiqdpufom1r2eTL7vDwepbo8RkUiSlUiRjRi1FbEmVhbTm1FnGe27OetQ50caO+mMhGjYVQF7pD2P7zxt3Vl3pQ6ulO0d6foSqaIWCaYzMh8ys1+0j3aWzn6ZgqQSqcZVZWgO5nm7IZapo6p4vTx1cc+VXcn0+xr62Lv4W6S6cwn5Tf3tRExoyIeZd+RLrpSaRpGVjB7Qg1n1ddSkYjS2ZNiy942tuxtywRaPMLIigR7j3TRuPUAbV1JKhNRohEjmXJGVmYOD46qTBCPRdi+v52pYyqpTETZvr+DVNrZ39bN3iNdRCKGu9PZk6YnlT72BhmLRo59qo9l2380RD0bmPFohKljKunsSVNTHqMyEaO1o4f97d10dKeYO6kGd2fz3jamjq5i9oQRRCMRaivivNJ8kBe2HqCjJ5UJ4qP7wZ1Z46uzv5MIW/e309rRQ1cyTUV2H4+sSJBKpznYkfnbmTK6kobRFXQn0+w42EE67VQkYuw53MmeQ12UxTOB2NaVxCEbwkdfL/P36DiGURaPUB6P0tGdIpr9UGJk/j5SaacrmT72oWNkZZxRlXFqKxOMrIgTjxrb93ewv72b6WOqiMeMw51J3thzhHE15YypSrDncBfdyczfyeVnThjw2YoKDgWHiEi/5BocOv9RRET6JVTBoSlHREQKL1TBoSlHREQKL1TBISIihafgEBGRfglVcGiMQ0Sk8EIVHBrjEBEpvFAFh4iIFF4oLwA0sxZg6wB/fCywN4/lBEltKT5haQeoLcVosO2Y6u51p1oplMExGGbWmMuVk6VAbSk+YWkHqC3FaKjaoUNVIiLSLwoOERHpFwXHO90ZdAF5pLYUn7C0A9SWYjQk7dAYh4iI9It6HCIi0i/DNjjMbImZbTSzJjO79TjfLzOzB7Lff8bMpg19lbnJoS2fNrMWM3sp+/hsEHWeipndbWZ7zGzdCb7b6nAGAAAE/UlEQVRvZnZbtp2vmNnCoa4xFzm0431m1tprf/z1UNeYKzObbGaPm9mrZrbezP7sOOuUyn7JpS1Fv2/MrNzMnjWzl7Pt+JvjrFPY9y93H3YPIAq8AcwAEsDLwNw+63wBuCP79TLggaDrHkRbPg18O+hac2jLe4CFwLoTfP+DwC/I3J3zfOCZoGseYDveBzwSdJ05tmUisDD7dTWw6Th/X6WyX3JpS9Hvm+zveUT26zjwDHB+n3UK+v41XHsci4Emd9/s7t3A/cDSPussBe7Nfv0QcIkV4q70g5dLW0qCu68B9p9klaXADzzjaWCkmU0cmupyl0M7Soa773L3F7JfHwY2APV9ViuV/ZJLW4pe9vd8JPs0nn30Hawu6PvXcA2OemB7r+fNvPMP6Ng67p4EWoExQ1Jd/+TSFoAPZw8jPGRmk4emtLzLta2l4ILsoYZfmNm8oIvJRfZwxwIyn3B7K7n9cpK2QAnsGzOLmtlLwB7gUXc/4T4pxPvXcA2O4eZnwDR3Pxt4lD98EpFgvEBmaodzgP8H/DTgek7JzEYAPwa+7O6Hgq5nME7RlpLYN+6ecvf5QAOw2MzOHMrXH67BsQPo/am7IbvsuOuYWQyoBfYNSXX9c8q2uPs+d+/KPv0+cO4Q1ZZvuey3oufuh44eanD3VUDczMYGXNYJmVmczBvtfe7+k+OsUjL75VRtKbV94+4HgceBJX2+VdD3r+EaHM8Bs8xsupklyAwereyzzkrgU9mvrwN+7dmRpiJzyrb0Od58FZlju6VoJfDJ7Fk85wOt7r4r6KL6y8wmHD3ebGaLyfw/LMYPJWTrvAvY4O7fOsFqJbFfcmlLKewbM6szs5HZryuADwCv9VmtoO9fsXxtqJS4e9LMbgFWkzkr6W53X29m/wtodPeVZP7AfmhmTWQGOpcFV/GJ5diWL5nZVUCSTFs+HVjBJ2FmK8ic1TLWzJqBb5AZ+MPd7wBWkTmDpwloB24IptKTy6Ed1wF/amZJoANYVqQfSgAuBD4BrM0eUwf4n8AUKK39Qm5tKYV9MxG418yiZILtQXd/ZCjfv3TluIiI9MtwPVQlIiIDpOAQEZF+UXCIiEi/KDhERKRfFBwiItIvCg6RIpOdofWRoOsQOREFh4iI9IuCQ2SAzOzj2fsivGRm381OPHfEzP5P9j4Jj5lZXXbd+Wb2dHaiyYfNbFR2+Uwz+1V2Ur0XzOy07OZHZCekfM3M7ivSmZllmFJwiAyAmZ0BXA9cmJ1sLgV8DKgic/XuPOAJMleNA/wA+Gp2osm1vZbfB9yenVTv3cDRqToWAF8G5pK518qFBW+USI6G5ZQjInlwCZnJIp/LdgYqyExxnQYeyK7zI+AnZlYLjHT3J7LL7wX+w8yqgXp3fxjA3TsBstt71t2bs89fAqYBTxa+WSKnpuAQGRgD7nX3r71todlf9VlvoHP6dPX6OoX+r0oR0aEqkYF5DLjOzMYBmNloM5tK5v/Uddl1Pgo86e6twAEzuyi7/BPAE9m70DWb2dXZbZSZWeWQtkJkAPQpRmQA3P1VM/s68EsziwA9wH8D2sjcWOfrZA5dXZ/9kU8Bd2SDYTN/mEH2E8B3szOb9gB/MoTNEBkQzY4rkkdmdsTdRwRdh0gh6VCViIj0i3ocIiLSL+pxiIhIvyg4RESkXxQcIiLSLwoOERHpFwWHiIj0i4JDRET65f8DnhYvfOS8f8oAAAAASUVORK5CYII=\n",
   "text/plain": "<Figure size 432x288 with 1 Axes>"
  },
  "metadata": {},
  "output_type": "display_data"
 }
]
```

## 结论

* Adadelta没有学习率参数。


## 练习

* Adadelta为什么不需要设置学习率参数？它被什么代替了？

**吐槽和讨论欢迎点**[这里](https://discuss.gluon.ai/t/topic/2277)
