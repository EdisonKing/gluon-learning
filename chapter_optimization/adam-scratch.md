# Adam --- 从0开始

Adam是一个组合了[动量法](momentum-scratch.md)和[RMSProp](rmsprop-scratch.md)的优化算法。



## Adam算法

Adam算法会使用一个动量变量$\mathbf{v}$和一个RMSProp中梯度按元素平方的指数加权移动平均变量$\mathbf{s}$，并将它们中每个元素初始化为0。在每次迭代中，首先计算[小批量梯度](gd-sgd-scratch.md) $\mathbf{g}$，并递增迭代次数

$$t := t + 1$$

然后对梯度做指数加权移动平均并计算动量变量$\mathbf{v}$:

$$\mathbf{v} := \beta_1 \mathbf{v} + (1 - \beta_1) \mathbf{g} $$


该梯度按元素平方后做指数加权移动平均并计算$\mathbf{s}$：

$$\mathbf{s} := \beta_2 \mathbf{s} + (1 - \beta_2) \mathbf{g} \odot \mathbf{g} $$


在Adam算法里，为了减轻$\mathbf{v}$和$\mathbf{s}$被初始化为0在迭代初期对计算指数加权移动平均的影响，我们做下面的偏差修正：

$$\hat{\mathbf{v}} := \frac{\mathbf{v}}{1 - \beta_1^t} $$

和

$$\hat{\mathbf{s}} := \frac{\mathbf{s}}{1 - \beta_2^t} $$



**可以看到，当$0 \leq \beta_1, \beta_2 < 1$时（算法作者建议分别设为0.9和0.999），当迭代后期$t$较大时，偏差修正几乎就不再有影响。**我们使用以上偏差修正后的动量变量和RMSProp中梯度按元素平方的指数加权移动平均变量，将模型参数中每个元素的学习率通过按元素操作重新调整一下：

$$\mathbf{g}^\prime := \frac{\eta \hat{\mathbf{v}}}{\sqrt{\hat{\mathbf{s}} + \epsilon}} $$

**其中$\eta$是初始学习率，$\epsilon$是为了维持数值稳定性而添加的常数，例如$10^{-8}$。和Adagrad一样，模型参数中每个元素都分别拥有自己的学习率。**

同样地，最后的参数迭代步骤与小批量随机梯度下降类似。只是这里梯度前的学习率已经被调整过了：

$$\mathbf{x} := \mathbf{x} - \mathbf{g}^\prime $$


## Adam的实现


Adam的实现很简单。我们只需要把上面的数学公式翻译成代码。

```{.python .input  n=1}
# Adam。
def adam(params, vs, sqrs, lr, batch_size, t):
    beta1 = 0.9
    beta2 = 0.999
    eps_stable = 1e-8
    for param, v, sqr in zip(params, vs, sqrs):      
        g = param.grad / batch_size
        v[:] = beta1 * v + (1. - beta1) * g
        sqr[:] = beta2 * sqr + (1. - beta2) * nd.square(g)
        v_bias_corr = v / (1. - beta1 ** t)
        sqr_bias_corr = sqr / (1. - beta2 ** t)
        div = lr * v_bias_corr / (nd.sqrt(sqr_bias_corr) + eps_stable)        
        param[:] = param - div
```

## 实验

实验中，我们以线性回归为例。其中真实参数`w`为[2, -3.4]，`b`为4.2。我们把算法中基于指数加权移动平均的变量初始化为和参数形状相同的零张量。

```{.python .input  n=2}
import mxnet as mx
from mxnet import autograd
from mxnet import ndarray as nd
from mxnet import gluon
import random

mx.random.seed(1)
random.seed(1)

# 生成数据集。
num_inputs = 2
num_examples = 1000
true_w = [2, -3.4]
true_b = 4.2
X = nd.random_normal(scale=1, shape=(num_examples, num_inputs))
y = true_w[0] * X[:, 0] + true_w[1] * X[:, 1] + true_b
y += .01 * nd.random_normal(scale=1, shape=y.shape)
dataset = gluon.data.ArrayDataset(X, y)

# 构造迭代器。
import random
def data_iter(batch_size):
    idx = list(range(num_examples))
    random.shuffle(idx)
    for batch_i, i in enumerate(range(0, num_examples, batch_size)):
        j = nd.array(idx[i: min(i + batch_size, num_examples)])
        yield batch_i, X.take(j), y.take(j)

# 初始化模型参数。
def init_params():
    w = nd.random_normal(scale=1, shape=(num_inputs, 1))
    b = nd.zeros(shape=(1,))
    params = [w, b]
    vs = []
    sqrs = []
    for param in params:
        param.attach_grad()
        # 把算法中基于指数加权移动平均的变量初始化为和参数形状相同的零张量。
        vs.append(param.zeros_like())
        sqrs.append(param.zeros_like())
    return params, vs, sqrs

# 线性回归模型。
def net(X, w, b):
    return nd.dot(X, w) + b

# 损失函数。
def square_loss(yhat, y):
    return (yhat - y.reshape(yhat.shape)) ** 2 / 2
```

接下来定义训练函数。当epoch大于2时（epoch从1开始计数），学习率以自乘0.1的方式自我衰减。训练函数的period参数说明，每次采样过该数目的数据点后，记录当前目标函数值用于作图。例如，当period和batch_size都为10时，每次迭代后均会记录目标函数值。

```{.python .input  n=3}
%matplotlib inline
import matplotlib as mpl
mpl.rcParams['figure.dpi']= 120
import matplotlib.pyplot as plt
import numpy as np

def train(batch_size, lr, epochs, period):
    assert period >= batch_size and period % batch_size == 0
    [w, b], vs, sqrs = init_params()
    total_loss = [np.mean(square_loss(net(X, w, b), y).asnumpy())]

    # 注意epoch从1开始计数。
    t = 0
    for epoch in range(1, epochs + 1):
        for batch_i, data, label in data_iter(batch_size):
            with autograd.record():
                output = net(data, w, b)
                loss = square_loss(output, label)
            loss.backward()
            # 必须在调用Adam前。
            t += 1
            adam([w, b], vs, sqrs, lr, batch_size, t)
            if batch_i * batch_size % period == 0:
                total_loss.append(np.mean(square_loss(net(X, w, b), y).asnumpy()))
        print("Batch size %d, Learning rate %f, Epoch %d, loss %.4e" % 
              (batch_size, lr, epoch, total_loss[-1]))
    print('w:', np.reshape(w.asnumpy(), (1, -1)), 
          'b:', b.asnumpy()[0], '\n')
    x_axis = np.linspace(0, epochs, len(total_loss), endpoint=True)
    plt.semilogy(x_axis, total_loss)
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.show()
```

使用Adam，最终学到的参数值与真实值较接近。

```{.python .input  n=4}
train(batch_size=10, lr=0.1, epochs=3, period=10)
```

```{.json .output n=4}
[
 {
  "name": "stdout",
  "output_type": "stream",
  "text": "Batch size 10, Learning rate 0.100000, Epoch 1, loss 8.4512e-04\nBatch size 10, Learning rate 0.100000, Epoch 2, loss 5.1735e-05\nBatch size 10, Learning rate 0.100000, Epoch 3, loss 4.9982e-05\nw: [[ 2.0014904 -3.4001632]] b: 4.202006 \n\n"
 },
 {
  "data": {
   "image/png": "iVBORw0KGgoAAAANSUhEUgAAAY4AAAEKCAYAAAAFJbKyAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMi4yLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvhp/UCwAAIABJREFUeJzt3Xt8XHWd//HXZ2ZyT5q0uZQ2SZveL1DaQqxcREDALUItKiC4uogIiyv+dNffLrDq7v5+q+tedP2tihdcEC/IRW6WWqyIl64IpWkp9BJKb7RNb0mvSZrmNvP9/THTkqZpMpNk5sycvJ+Pxzw6c3Jyzud02nnP93y/53vMOYeIiEi8Al4XICIimUXBISIiCVFwiIhIQhQcIiKSEAWHiIgkRMEhIiIJUXCIiEhCFBwiIpIQBYeIiCQk5HUByVBWVuZqamq8LkNEJKOsXr36gHOufKD1fBkcNTU11NXVeV2GiEhGMbMd8aynU1UiIpIQXwWHmS0ys/uPHj3qdSkiIr7lq+Bwzj3rnLujuLjY61JERHzLV8EhIiLJp+AQEZGEKDhERCQhCg4REUmIgqOHldsO8sMXt3tdhohIWkv7CwDNbDLwBaDYOXd9Mvf189UNPLG6gbbOMJ++fGoydyUikrE8aXGY2YNm1mhm63stX2hmm8xsi5ndA+Cc2+acuy0Vdf3rB+eweN54/mP5Jr7x/Js451KxWxGRjOLVqaqHgIU9F5hZELgPuBqYDdxsZrNTWVQoGOA/b5zHDedX8V8vbOZfltUrPEREevHkVJVzboWZ1fRavADY4pzbBmBmjwKLgY2prC0YMP7tQ+eSnx3kB/+znbbOMF++7hzMLJVliIikrXTqHK8EdvV43QBUmlmpmX0PmG9m957pl83sDjOrM7O6pqamIRUSCBj/9P6zufPSKTy8cif/vFQtDxGRE9K+c9w5dxC4M4717jezvcCi7Ozs84e6XzPj7oUz6OgO8+CL2ynKDfHXV00f6mZFRDJeOrU4dgPVPV5XxZbFbbjnqjIzvnTNbG6sjfZ5/GDFtmHZrohIJkunFscqYJqZTSIaGDcBH0lkA2a2CFg0derwDaUNBIyvfvBcjnWE+cqyegpzQ9y8YMKwbV9EJNN4NRz3EeAlYIaZNZjZbc65buAuYDlQDzzunNuQyHaTNTtuMGB848PzuHxGOV94eh2/3rBvWLcvIpJJzE+dvj1aHLdv3rx52Ld/vDPMTT94mU37mnn0jguZV10y7PsQEfGKma12ztUOtF469XEMWbLvx5GXHeSBW2opL8rhtodWsfNgW1L2IyKSznwVHKlQVpjDQ7cuIOwcH//hKxw+1ul1SSIiKeWr4EjVrWOnlBfyg7+opeHIcW7/cR3tXeGk7k9EJJ34KjhSeevYd9SM4Rs3zqNux2E+//hrRCL+6SsSEelPOg3HzTjXnDuOPUdm8ZVl9VSNzuPe983yuiQRkaTzVXAk4zqOgXzykknsOtzG91dso3J0Hn9xYU3K9i0i4gWdqhoiM+MfF53NlbMq+KclG3h+4/6U7VtExAu+Cg6vBAPGN2+ez5zKYj7zyBrW705u57yIiJd8FRypGlXVl/zsEP99yzsYk5/N7T+uo7G5PeU1iIikgq+Cw4tTVT2VF+Xwg1tqOdLWxR0/Wa1huiLiS74KjnRw9vhivvHhuazddYR7n1qn+3iIiO8oOJJg4Tnj+JurpvP0q7v5ycs7vC5HRGRYKTiS5K7Lp/KemRV8eWk96xrUWS4i/uGr4PCyc7y3QMD4+g1zKS3M5tM/W0Nze5fXJYmIDAtfBYfXneO9jS7I5tsfmc/uI8e5+4nX1d8hIr7gq+BIR+dPHMPf/dkMnlu/jx+/pP4OEcl8Co4UuP2SyVwxs4Kv/LKeDXu8P40mIjIUCo4UCASMr90wl5L8LP76sbW6vkNEMpqCI0VGF2Tz79efy5v7W/na8k1elyMiMmi+Co50GlXVl8tmVPDRCybwwIvbeWnrQa/LEREZFF8FR7qNqurL379vFjWlBfzvn7+mIboikpF8FRyZID87xNdvnMveo8f5t+fe8LocEZGEKTg8cN6E0Xz8okk8vHInq9465HU5IiIJUXB45PPvnU5lSR73PrWOjm6NshKRzKHg8EhBTogvf+ActjS28t3fb/W6HBGRuCk4PHT5jAreP3c83/ndVrY0tnhdjohIXBQcHvuHRbPJzwlyz5PriEQ0l5WIpL+0Dw4zKzCzH5nZD8zsz72uZ7iVFebwhffNom7HYX72yk6vyxERGZAnwWFmD5pZo5mt77V8oZltMrMtZnZPbPEHgSecc7cD7095sSlw/flVXDy1lH977g32HdW9ykUkvXnV4ngIWNhzgZkFgfuAq4HZwM1mNhuoAnbFVvPl8CMz4yvXzaEjHOEry+q9LkdEpF+eBIdzbgXQ+wKGBcAW59w251wn8CiwGGggGh7QT71mdoeZ1ZlZXVNTUzLKTqqasgL+6rIpPPvaHv605YDX5YiInFE69XFU8nbLAqKBUQk8BXzIzL4LPHumX3bO3e+cq3XO1ZaXlye30iS589IpVI/J4x+WbKCzO+J1OSIifUqn4OiTc+6Yc+5W59ynnHMP97duuk9yOJDcrCD/tOhstjS28tCftntdjohIn9IpOHYD1T1eV8WWxS0TJjkcyBWzxnLlrAr+3282q6NcRNJSOgXHKmCamU0ys2zgJmBJIhvI9BbHCf+46GzCEaeOchFJS14Nx30EeAmYYWYNZnabc64buAtYDtQDjzvnNiSyXT+0OACqx+TzqRMd5VvVUS4i6cWc88/Vyma2CFg0derU2zdv3ux1OUPS3hXmiq//geK8LJ79zLsIBszrkkTE58xstXOudqD10ulU1ZD5pcUB0Y7yu6+eyca9zTy5psHrckRETvJVcPilj+OEReeOY151CV9bvom2zm6vyxERAXwWHH5qcUD0ivIvXTuLxpYOvv+HbV6XIyIC+Cw4/Oj8iWO4Zs447l+xTcNzRSQt+Co4/Haq6oS7F86kOxLhv1540+tSRET8FRx+O1V1woTSfG5eMIGf1zWw4+Axr8sRkRHOV8HhZ5++fCrBgPFfL2T2MGMRyXwKjgwxdlQut1xUwzOv7tZtZkXEU74KDr/2cZzwl++eTF5WkG88r1aHiHjHV8Hh1z6OE0oLc/jEuybxy3V72bDHn+EoIunPV8ExEnzykskU5Ya473dbvC5FREYoBUeGKc7L4mMXTOS59fvY1tTqdTkiMgL5Kjj83sdxwq0XTyIrGOD+FbqaXERSz1fB4fc+jhPKi3K4sbaKp9bsZn+zriYXkdTyVXCMJHdcMoXuSIQH/6hbzIpIaik4MtSE0nyuPXc8P315B0fburwuR0RGEAVHBrvz0ikc6wzz05U7vC5FREYQBUcGmz1+FJdMK+MnL+2gKxzxuhwRGSEUHBnu1otr2Nfczq/W7/O6FBEZIXwVHCNlOG5Pl02vYGJpPj98UZ3kIpIavgqOkTIct6dAwLjlwhrW7DzCa7uOeF2OiIwAvgqOkeqG2ioKsoP8+CV1kotI8ik4fKAoN4vF8ytZ+voeDc0VkaRTcPjERxZMoKM7wjNrd3tdioj4nILDJ86pLGZOZTGPvLIT55zX5YiIjyk4fOQj75zAG/taeFWd5CKSRGkfHGY22cweMLMnvK4l3S2aO56C7CCPvrLT61JExMeSGhxm9qCZNZrZ+l7LF5rZJjPbYmb39LcN59w259xtyazTLwpzQlw9ZxzL1u3jeGfY63JExKeS3eJ4CFjYc4GZBYH7gKuB2cDNZjbbzOaY2dJej4ok1+c7HzyvktaObn69UVeSi0hyhJK5cefcCjOr6bV4AbDFObcNwMweBRY7574KXJvMekaCCyaVMr44l6fW7GbxvEqvyxERH/Kij6MS2NXjdUNsWZ/MrNTMvgfMN7N7+1nvDjOrM7O6pqam4as2wwQCxgfOq+R/NjfRqJs8iUgSpH3nuHPuoHPuTufclFir5Ezr3e+cq3XO1ZaXl6eyxLTzgflVRBz8Yu0er0sRER/yIjh2A9U9XlfFlg3ZSJzksC9TKwqZW13Ck2savC5FRHzIi+BYBUwzs0lmlg3cBCzxoA5f+9B5lbyxr4WNe5q9LkVEfCbZw3EfAV4CZphZg5nd5pzrBu4ClgP1wOPOuQ3Dsb+RODvumSw6dzxZQeMptTpEZJgle1TVzWdYvgxYlsx9j3SjC7K5dHoFS1/fy9+/bxaBgHldkoj4RNp3jidCfRynWjR3HPua21m987DXpYiIj/gqOHSq6lRXzhpLblaAZ1/T6CoRGT6+Cg61OE5VkBPiPTMrWLZuL93hiNfliIhP+Co41OI43aJzx3OgtZOV2w95XYqI+ISvgkNOd/nMCgqygyx9XaerRGR4+Co4dKrqdLlZQS6bWcHzGxuJRHSDJxEZOl8Fh05V9e29s8dyoLWD1xp0gycRGTpfBYf07bLpFQQDxvMb93tdioj4gK+CQ6eq+lacn8WCmjH8pl7BISJDF1dwmNlnzWyURT1gZmvM7L3JLi5ROlV1ZlfOHsub+1vZcfCY16WISIaLt8XxCedcM/BeYDTwMeBfk1aVDLurZo0F0OkqERmyeIPjxERH7wN+EpuUUJMfZZAJpflMH1vIC/WNXpciIhku3uBYbWa/Jhocy82sCNClyBnmshkV1O04xLGObq9LEZEMFm9w3AbcA7zDOdcGZAG3Jq2qQVLneP8unV5OV9jx8raDXpciIhks3uC4ENjknDtiZh8Fvgik3aezOsf7V1szmrysIH94c+Tek11Ehi7e4Pgu0GZmc4HPA1uBHyetKkmKnFCQC6eUKjhEZEjiDY5u55wDFgPfds7dBxQlryxJlkunl7PjYBtvHdCwXBEZnHiDo8XM7iU6DPeXZhYg2s8hGebS6eUArNisVoeIDE68wfFhoIPo9Rz7gCrgP5JWlSRNTVkBE0vzWaHTVSIySHEFRywsHgaKzexaoN05l3Z9HBpVFZ+LppSycvshwpotV0QGId4pR24EXgFuAG4EVprZ9cksbDA0qio+75xUSkt7N/V7m70uRUQyUCjO9b5A9BqORgAzKwd+AzyRrMIked45eQwAL287yDmVClkRSUy8fRyBE6ERczCB35U0M644j4ml+by8TbeTFZHExdvi+JWZLQceib3+MLAsOSVJKlwwqZRfbdhHJOIIBDTtmIjEL97O8b8F7gfOjT3ud87dnczCJLneOXkMR493Ub9P/Rwikph4Wxw4554EnkxiLZJC75xcCsDKbYc4e7z6OUQkfv22OMysxcya+3i0mJm+qmawypI8qsfkacJDEUlYvy0O51xaTCtiZtcB1wCjgAecc7/2uCRfqJ04hj9uOYBzDjP1c4hIfJI+MsrMHjSzRjNb32v5QjPbZGZbzOye/rbhnHvGOXc7cCfRjnkZBnOrimlq6WBfc7vXpYhIBknFkNqHgIU9F5hZELgPuBqYDdxsZrPNbI6ZLe31qOjxq1+M/Z4Mg7nVJQC8tuuIx5WISCaJu3N8sJxzK8ysptfiBcAW59w2ADN7FFjsnPsqcG3vbVj0PMq/As8559b0tR8zuwO4A2DChAnDVr+fzRo3iqygsXbXURaeM87rckQkQ3h1EV8lsKvH64bYsjP5DHAlcL2Z3dnXCs65+51ztc652vLy8uGr1Mdys4LMHjdKLQ4RSUjSWxzDwTn3TeCbA61nZouARVOnTk1+UT4xt7qEp9bsJhxxBHUhoIjEwasWx26gusfrqtiyIdEkh4mbW1VCa0c325pavS5FRDKEV8GxCphmZpPMLBu4CVgy1I1qWvXEneggf1Wnq0QkTqkYjvsI8BIww8wazOw251w3cBewHKgHHnfObRjqvtTiSNzksgKKckLq5xCRuKViVNXNZ1i+jGGeKFF9HIkLBIxzq4t5vUGtNBGJj6+mRleLY3DOHl/Mpv0tdIUjXpciIhnAV8GhPo7BmT1uFJ3dEbY1HfO6FBHJAL4KDrU4Bufs8aMA2LBHgSsiA/NVcMjgTCorICcUYOMeTXgsIgPzVXDoVNXghIIBZp5VxMa9Cg4RGZivgkOnqgZv+tgiNjfqIkARGZivgkMGb2pFIU0tHRw93uV1KSKS5hQcAsCU8kIAtmrqEREZgK+CQ30cgzelIhocW3S6SkQG4KvgUB/H4FWPziM7GFCLQ0QG5KvgkMELBQPUlOWztVEXAYpI/zLifhySGlMrCqnf2zKkbTjneHN/Ky9vO0hjSzuj87M5f+Jo5lWXEL2Ro4hkOl8FhyY5HJop5YUs37Cfju4wOaFgwr//5v4WvvTMelZuPwSAGTgX/dnMs4q4e+FMLp9Z0c8WRCQT+Co4nHPPAs/W1tbe7nUtmWhqRSHhiGPHwTamjy1K6Hd/v6mRv3p4DblZQb54zSyunjOO8cW5HGjt5HdvNPLdP2zl1odWcdmMcr5+w1xKC3OSdBQikmzq45CTTg7JTXBk1as7D3PnT1czqayA5z57CZ+8ZDKVJXmYGeVFOdz4jmqWf+7dfPGaWby87SDXfedFtjQO7ZSYiHhHwSEnTS4vABIbktvW2c3nHltLWWEOP/rEAsaOyu1zvexQgE9eMplH77iQ450RPvCdP/F6g24eJZKJFBxyUn52iMqSvISG5H7rt1vYcbCN/7h+LmVxnH6aV13CM5++iOK8LG558BU271fLQyTTKDjkFJPLC9gSZ3A0tXTw0ItvsXjeeC6cUhr3PqpG5/PwJ99JKBjgow+sZO/R44MtV0Q8oOCQU0wuK+CtA224E8Oh+vHff9xGR3eYz14xLeH9TCwt4Ce3LaC1vZtP/XQNHd3hwZQrIh7wVXBoypGhqykroLWjmwOtnf2u194V5vFVu3jv7LOYHOtUT9TMs0bxtRvmsnbXEb68tH5Q2xCR1PNVcGjKkaGrKYt2kL91sP8ryJ9bv5fDbV189IKJQ9rf1XPG8ZfvnsxPXt7BsnV7h7QtEUkNXwWHDN2k0mhwbB/g/uNPrdlN9Zg8Lkqgb+NM/vbPZjC3qpgvPL2OppaOIW9PRJJLwSGnqBqdRyhgbO+nxXH4WCd/2nqQa+aMJxAY+jQioWCAr984l2OdYe59al1c/Ssi4h0Fh5wiFAwwYUw+bx04c3D8euM+whHHNXPGDdt+p1YU8Xd/NoPf1O/nF2v3DNt2RWT4KTjkNDVlBWzvJzheqG+ksiSPcypHDet+b714EvOqS/jnpRs50tZ/57yIeEfBIaepKS1gx8G+h+R2hSO8tPUg755ePuyz3QYDxlc/OIcjx7v4l2UaZSWSrtI+OMxslpl9z8yeMLNPeV3PSDCpLJ/jXWH2N5/eUb121xFaOrp597SypOx71rhRfPKSSTxe18Cqtw4lZR8iMjRJDQ4ze9DMGs1sfa/lC81sk5ltMbN7+tuGc67eOXcncCNwcTLrlagTQ3L7Ol31P5sPEDC4aGpyggPgs1dMY+yoHP5lWb06ykXSULJbHA8BC3suMLMgcB9wNTAbuNnMZpvZHDNb2utREfud9wO/BJYluV4BJvUTHKu2H+Ls8cUU52Ulbf/52SH+5qrpvLrzCL9avy9p+xGRwUlqcDjnVgC9zzcsALY457Y55zqBR4HFzrl1zrlrez0aY9tZ4py7GvjzZNYrUeOL88gOBU67CLA7HOG1hiOcP3F00mv40HlVTKso5N+Xb6IrHEn6/kQkfl70cVQCu3q8bogt65OZXWZm3zSz79NPi8PM7jCzOjOra2pqGr5qR6BAwJg4Jv+0Fsem/S20dYaZP6Ek6TWEggHuXjiT7QeO8eTqhqTvT0Til/Z3AHTO/R74fRzr3Q/cD1BbW6sT40NUU1Zw2rUca3YcBuC8CclvcQBcMauCuVXFfOf3W7n+/CpCwbQfyyEyInjxP3E3UN3jdVVs2ZBpksPhM6msgB2H2ohE3s7gNTuPUFGUQ9XovJTUYGbc9Z5p7DzUxrOv66JAkXThRXCsAqaZ2SQzywZuApZ4UIf0o6a0gM7uCHt63Ctj9Y7DnDdh9LBfv9GfK2dVMPOsIr792y2EI2pIiqSDZA/HfQR4CZhhZg1mdptzrhu4C1gO1AOPO+c2DMf+NDvu8KkpywfgrQNtQPSmTTsPtaWkY7wnM+Mz75nG1qZjLN+gEVYi6SCpfRzOuZvPsHwZGlqb1iaXRe+xsf1AK++aVsaanbH+jYnJ7xjvbeE5ZzFhTD4P/nE77xvG+bFEZHB81duoPo7hM3ZUDnlZQbbHWhxrdh4mK2icPT71rblgwPj4RTXU7TjMa7uOpHz/InIqXwWHTlUNHzNjYmn+yWs5Xt52iLlVJeRmBT2p54baKgpzQvzwxe2e7F9E3uar4FCLY3hNKS9k074Wmtu7WNdwZFhu2jRYRblZ3FhbzdLX97K/ud2zOkTEZ8GhFsfwunBKKbuPHOeRlTuJOLhwSvLmp4rHxy+qIewcP1u509M6REY6XwWHDK8rZlUA8NXn3iAnFEjJFeP9mVCaz7umlvHzul0amiviIV8Fh05VDa9xxXkU5UYH3n3iXZM869/o6eYFE9hztJ0Vb2paGRGv+Co4dKpq+H3r5vl8/qrp/O17Z3hdCgBXzhpLaUE2j7yi01UiXvFVcMjwu2xGBZ+5YhqBQOquFu9PdijA9edX8cIbjTSqk1zEE74KDp2qGhk+/I5qwhHHk2uGZYozEUmQr4JDp6pGhsnlhZw3oYSnX23QHQJFPOCr4JCR4wPzK3lzfyv1e1u8LkVkxFFwSEa65tzxhALGM2t1ukok1RQckpHGFGRz2Yxylqzdo2s6RFLMV8GhzvGR5br5lexrbmfltoNelyIyovgqONQ5PrJcOWsshTkhnn5Vp6tEUslXwSEjS25WkKvPOYvn1u+jvSvsdTkiI4aCQzLaB+ZX0trRzfMb93tdisiIoeCQjPbOyaWMHZXDLzS6SiRlFByS0YIBY/G8Sn6/qYlDxzq9LkdkRPBVcGhU1ci0eN54uiOOX67b63UpIiOCr4JDo6pGptnjRjGtopBfaHSVSEr4KjhkZDIzrptfSd2Ow+w61OZ1OSK+p+AQX1g8bzyAOslFUkDBIb5QNTqfBTVjePrV3ZoxVyTJFBziG4vnj2dr0zE27Gn2uhQRX1NwiG9cM2ccWUHjGXWSiySVgkN8oyQ/m8tmVLDkNc2YK5JMGREcZlZgZnVmdq3XtUh6+8D8ShpbOnhpq2bMFUmWpAaHmT1oZo1mtr7X8oVmtsnMtpjZPXFs6m7g8eRUKX7ynpkVjMoN8VjdLq9LEfGtZLc4HgIW9lxgZkHgPuBqYDZws5nNNrM5Zra016PCzK4CNgKNSa5VfCA3K8gNtdU8t24vjc3tXpcj4ktJDQ7n3ArgUK/FC4AtzrltzrlO4FFgsXNunXPu2l6PRuAy4ALgI8DtZtZnzWZ2R+x0Vl1TU1PyDkrS3kcvmEh3xPHIK2p1iCSDF30clUDP/9ENsWV9cs59wTn3OeBnwA+cc5EzrHe/c67WOVdbXl4+rAVLZplUVsCl08v52Ss76Ar3+c9FRIYgIzrHAZxzDznnlva3jiY5lBNuuWgi+5s7WL5hn9eliPiOF8GxG6ju8boqtmzINMmhnHDp9AomlRXw7d9uIaKhuSLDyovgWAVMM7NJZpYN3AQsGY4Nq8UhJwQDxueunMYb+1pY8toer8sR8ZVkD8d9BHgJmGFmDWZ2m3OuG7gLWA7UA4875zYMx/7U4pCeFp07njmVxfzz0o0cbO2I63eOtHXyuzcaeWzVTn768g5+sXY3G/Yc1T3NRXowP00IZ2aLgEVTp069ffPmzV6XI2lg074WFn3rj8yrLuGHt76DgpzQKT93zrG1qZXf1DfyQv1+Vu84TF9ntrKCxtnji7lkWhk31lZTPSY/RUcgkjpmtto5Vzvgen4KjhNqa2tdXV2d12VImnj2tT189tFXqRydxx2XTGbmuFHsPNjG2l1HWLG5iR0Ho/fwOHv8KK6YNZaLp5RSNSafrIBxuK2LN/e3sH7PUdbsOMzqHYdxwLunlXNjbTWXzywnPzvUfwEiGULBoeCQHv609QBf+WX9KTPnFmQHWTBpDFfMGssVsyoYV5w34Hb2HDnOY6t28diqXexrbicnFOCiKaXMqx7N3OpizqksprQgGzNL5uGIJMWIDA6dqpL+RE9LHWPv0eNUFOUytaKQYGBwH/DhiOOV7Yf41fq9vLj1IFubWjnxX2lUbohJ5YVMHJPPuJJczhqVy7jiXMaOyqViVC7lhTlkhzJmJLyMICMyOE5Qi0NSraW9i3W7j1K/t4W3Dhxj+4Fj7Dh0jP1HO+js4yLE0flZVBTlUjEqh/LCHMpH5URfF+VQUZRDeVEOZUU55GcFCQXfDpmucIR9R9tpOHyc3UeOs/vwcXYfaWP3kePsOdLO0eNdtHZ0U5QToqwwtp3C7NifOaf9OaYg+2R4Oudobu/mQGsHTS0dffzZSVNLBy3tXXR0R8gOBRhblBurPbq94rwsSvKzKM6LPkrysinOy6IoN0RggJB2zhFxEHEu+oj0eO6iPw8GjFAgQDBgBANGwBi21l0k4ugMR+jojtDZHaEzHPuzO0JXbHlXOIJzxPYPwUCAUKyWUMAIBQMEjJPH4U7W/vaxvP387WMOBYzR+dmMLsiiMCfU7zFFIo7jXWGOdXTT2vPR3s2xzm4umVZOWWHOoP4ORmRwqMUh6cY5x6Fjnew92s7+5nYaWzpobO6gqbWdxuYOGluiH8pNLX0HDEQ/pLKCRnfY0d1Hz31FUQ6Vo/MYX5LHmPxs8rODtHR0n/Kh39TSQUf36dsPGIwpyMY5aG7voit8+vaDAaO04O3wGZWXRW4oQHt3hMbmdppaOtjX3E5b55lHnplBXlYQ58AR/bDkxIcrnPxAHYxQwAgEjN4ftWYQMCNgdvL528uigdPdIyj6+rv1QjBg5IYC5GYFyc0KEgoaHV0RjneFae8K9/k+9vTI7Rdw4ZTSQe17RAbHCWpxSKZxznH0eNcpwXKwtZPjndEPis5whFDAyM0KctaoXCpH51FZkse4klxyQsG4tt/a0X2y1XCg9dQWRcCMUXlZjMnv3TrJZnR+9oCtBYD2rjBHj3dx9HiTBwRFAAAGy0lEQVQXR9q6ejzvpPl4F22d4ZMf3Jz4IKf3B3r0m7z1eB79eXTdiIuGZzji6A47ws4RjpzhQ7/HN/ue3/R7fuMPBYycUIDsno9g4NRlwSDZoQBZQSM7GACDSATCzhE5UUskQlc4+jwcibaMTg+ut48r+vzt113dEQ63dXK4rZOjx7to74rQ3hWmvSvaysnLCpKbFSA3O0huKEhedpCCnBBFOSEKckIUxh4FOUHGl+SRmzXwv4m+xBscGg4ikgbMjJL8bErys5k+tigp2y/KzaIoN4tJZQXDvn3g5DfksaNyk7J9SR/qoRMRkYT4Kjg05YiISPL5Kjg05YiISPL5KjhERCT5FBwiIpIQXwWH+jhERJLPV8GhPg4RkeTzVXCIiEjy+fLKcTNrAnYM8tfLgAPDWI6XdCzpxy/HATqWdDTU45jonCsfaCVfBsdQmFldPJfcZwIdS/rxy3GAjiUdpeo4dKpKREQSouAQEZGEKDhOd7/XBQwjHUv68ctxgI4lHaXkONTHISIiCVGLQ0REEjJig8PMFprZJjPbYmb39PHzHDN7LPbzlWZWk/oq4xPHsXzczJrMbG3s8Ukv6hyImT1oZo1mtv4MPzcz+2bsOF83s/NSXWM84jiOy8zsaI/34x9SXWO8zKzazH5nZhvNbIOZfbaPdTLlfYnnWNL+vTGzXDN7xcxeix3H/+ljneR+frnYfXFH0gMIAluByUA28Bowu9c6fwV8L/b8JuAxr+sewrF8HPi217XGcSzvBs4D1p/h5+8DngMMuABY6XXNgzyOy4ClXtcZ57GMA86LPS8C3uzj31emvC/xHEvavzexv+fC2PMsYCVwQa91kvr5NVJbHAuALc65bc65TuBRYHGvdRYDP4o9fwK4wvq7g7x34jmWjOCcWwEc6meVxcCPXdTLQImZjUtNdfGL4zgyhnNur3NuTex5C1APVPZaLVPel3iOJe3F/p5bYy+zYo/endVJ/fwaqcFRCezq8bqB0/8BnVzHOdcNHAUGdwf45IrnWAA+FDuN8ISZVaemtGEX77FmggtjpxqeM7OzvS4mHrHTHfOJfsPtKePel36OBTLgvTGzoJmtBRqB551zZ3xPkvH5NVKDY6R5Fqhxzp0LPM/b30TEG2uITu0wF/gW8IzH9QzIzAqBJ4HPOeeava5nKAY4lox4b5xzYefcPKAKWGBm56Ry/yM1OHYDPb91V8WW9bmOmYWAYuBgSqpLzIDH4pw76JzriL38b+D8FNU23OJ539Kec675xKkG59wyIMvMyjwu64zMLIvoB+3Dzrmn+lglY96XgY4l094b59wR4HfAwl4/Surn10gNjlXANDObZGbZRDuPlvRaZwlwS+z59cBvXaynKc0MeCy9zje/n+i53Uy0BPiL2CieC4Cjzrm9XheVKDM768T5ZjNbQPT/YTp+KSFW5wNAvXPuP8+wWka8L/EcSya8N2ZWbmYlsed5wFXAG71WS+rnV2i4NpRJnHPdZnYXsJzoqKQHnXMbzOz/AnXOuSVE/4H9xMy2EO3ovMm7is8szmP5X2b2fqCb6LF83LOC+2FmjxAd1VJmZg3APxLt+MM59z1gGdERPFuANuBWbyrtXxzHcT3wKTPrBo4DN6XplxKAi4GPAeti59QB/h6YAJn1vhDfsWTCezMO+JGZBYkG2+POuaWp/PzSleMiIpKQkXqqSkREBknBISIiCVFwiIhIQhQcIiKSEAWHiIgkRMEhkmZiM7Qu9boOkTNRcIiISEIUHCKDZGYfjd0XYa2ZfT828VyrmX0jdp+EF8ysPLbuPDN7OTbR5NNmNjq2fKqZ/SY2qd4aM5sS23xhbELKN8zs4TSdmVlGKAWHyCCY2Szgw8DFscnmwsCfAwVEr949G/gD0avGAX4M3B2baHJdj+UPA/fFJtW7CDgxVcd84HPAbKL3Wrk46QclEqcROeWIyDC4guhkkatijYE8olNcR4DHYuv8FHjKzIqBEufcH2LLfwT83MyKgErn3NMAzrl2gNj2XnHONcRerwVqgD8m/7BEBqbgEBkcA37knLv3lIVmX+q13mDn9Ono8TyM/q9KGtGpKpHBeQG43swqAMxsjJlNJPp/6vrYOh8B/uicOwocNrNLYss/Bvwhdhe6BjO7LraNHDPLT+lRiAyCvsWIDIJzbqOZfRH4tZkFgC7g08AxojfW+SLRU1cfjv3KLcD3YsGwjbdnkP0Y8P3YzKZdwA0pPAyRQdHsuCLDyMxanXOFXtchkkw6VSUiIglRi0NERBKiFoeIiCREwSEiIglRcIiISEIUHCIikhAFh4iIJETBISIiCfn/46eDW2MUKsQAAAAASUVORK5CYII=\n",
   "text/plain": "<Figure size 432x288 with 1 Axes>"
  },
  "metadata": {},
  "output_type": "display_data"
 }
]
```

## 结论

* Adam组合了动量法和RMSProp。


## 练习

* 你是怎样理解Adam算法中的偏差修正项的？

**吐槽和讨论欢迎点**[这里](https://discuss.gluon.ai/t/topic/2279)
