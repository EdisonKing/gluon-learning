# Adagrad --- 从0开始


在我们之前的优化算法中，无论是梯度下降、随机梯度下降、小批量随机梯度下降还是使用动量法，模型参数中的每一个元素在相同时刻都使用同一个学习率来自我迭代。

举个例子，当一个模型的损失函数为$L$，参数为一个多维向量$[x_1, x_2]^\top$时，该向量中每一个元素在更新时都使用相同的学习率，例如在学习率为$\eta$的梯度下降中：

$$
x_1 := x_1 - \eta \frac{\partial{L}}{\partial{x_1}} \\
x_2 := x_2 - \eta \frac{\partial{L}}{\partial{x_2}}
$$

其中元素$x_1$和$x_2$都使用相同的学习率$\eta$来自我迭代。如果让$x_1$和$x_2$使用不同的学习率自我迭代呢？


**Adagrad就是一个在迭代过程中不断自我调整学习率，并让模型参数中每个元素都使用不同学习率的优化算法。**


## Adagrad算法

由于小批量随机梯度下降包含了梯度下降和随机梯度下降这两种特殊形式，我们在之后的优化章节里提到的梯度都指的是小批量随机梯度。由于我们会经常用到按元素操作，这里稍作介绍。假设$\mathbf{x} = [4, 9]^\top$，以下是一些按元素操作的例子：

* 按元素相加： $\mathbf{x} + 1 = [5, 10]^\top$
* 按元素相乘： $\mathbf{x} \odot \mathbf{x} = [16, 81]^\top$
* 按元素相除： $72 / \mathbf{x} = [18, 8]^\top$
* 按元素开方： $\sqrt{\mathbf{x}} = [2, 3]^\top$

Adagrad的算法会使用一个梯度按元素平方的累加变量$\mathbf{s}$，并将其中每个元素初始化为0。在每次迭代中，首先计算[小批量梯度](gd-sgd-scratch.md) $\mathbf{g}$，然后将该梯度按元素平方后累加到变量$\mathbf{s}$：

$$\mathbf{s} := \mathbf{s} + \mathbf{g} \odot \mathbf{g} $$

然后我们将模型参数中每个元素的学习率通过按元素操作重新调整一下：

$$\mathbf{g}^\prime := \frac{\eta}{\sqrt{\mathbf{s} + \epsilon}} \odot \mathbf{g} $$

其中$\eta$是初始学习率，$\epsilon$是为了维持数值稳定性而添加的常数，例如$10^{-7}$。请注意其中按元素开方、除法和乘法的操作。这些按元素操作使得模型参数中每个元素都分别拥有自己的学习率。

需要强调的是，由于梯度按元素平方的累加变量$\mathbf{s}$出现在分母，**Adagrad的核心思想是：如果模型损失函数有关一个参数元素的偏导数一直都较大，那么就让它的学习率下降快一点，即学习率相对小一点；反之，如果模型损失函数有关一个参数元素的偏导数一直都较小，那么就让它的学习率下降慢一点，即学习率相对大一点。然而，由于$\mathbf{s}$一直在累加按元素平方的梯度，每个元素的学习率在迭代过程中一直在降低或不变。所以在有些问题下，当学习率在迭代早期降得较快时且当前解依然不理想时，Adagrad在迭代后期可能较难找到一个有用的解。**

最后的参数迭代步骤与小批量随机梯度下降类似。只是这里梯度前的学习率已经被调整过了：

$$\mathbf{x} := \mathbf{x} - \mathbf{g}^\prime $$




## Adagrad的实现

Adagrad的实现很简单。我们只需要把上面的数学公式翻译成代码。

```{.python .input  n=1}
# Adagrad算法
def adagrad(params, sqrs, lr, batch_size):
    eps_stable = 1e-7
    for param, sqr in zip(params, sqrs):
        g = param.grad / batch_size
        sqr[:] += nd.square(g)
        div = lr * g / nd.sqrt(sqr + eps_stable)
        param[:] -= div
```

## 实验

实验中，我们以线性回归为例。其中真实参数`w`为[2, -3.4]，`b`为4.2。我们把梯度按元素平方的累加变量初始化为和参数形状相同的零张量。

```{.python .input  n=2}
from mxnet import ndarray as nd
import mxnet as mx
from mxnet import autograd
from mxnet import gluon
import random

mx.random.seed(1)
random.seed(1)

# 生成数据集。
num_inputs = 2
num_examples = 1000
true_w = [2, -3.4]
true_b = 4.2
X = nd.random_normal(scale=1, shape=(num_examples, num_inputs))
y = true_w[0] * X[:, 0] + true_w[1] * X[:, 1] + true_b
y += .01 * nd.random_normal(scale=1, shape=y.shape)
dataset = gluon.data.ArrayDataset(X, y)

# 构造迭代器。
import random
def data_iter(batch_size):
    idx = list(range(num_examples))
    random.shuffle(idx)
    for batch_i, i in enumerate(range(0, num_examples, batch_size)):
        j = nd.array(idx[i: min(i + batch_size, num_examples)])
        yield batch_i, X.take(j), y.take(j)

# 初始化模型参数。
def init_params():
    w = nd.random_normal(scale=1, shape=(num_inputs, 1))
    b = nd.zeros(shape=(1,))
    params = [w, b]
    sqrs = []
    for param in params:
        param.attach_grad()
        # 把梯度按元素平方的累加变量初始化为和参数形状相同的零张量。
        sqrs.append(param.zeros_like())
    return params, sqrs

# 线性回归模型。
def net(X, w, b):
    return nd.dot(X, w) + b

# 损失函数。
def square_loss(yhat, y):
    return (yhat - y.reshape(yhat.shape)) ** 2 / 2
```

接下来定义训练函数。训练函数的period参数说明，每次采样过该数目的数据点后，记录当前目标函数值用于作图。例如，当period和batch_size都为10时，每次迭代后均会记录目标函数值。

另外，与随机梯度下降算法不同，这里的初始学习率`lr`没有自我衰减。

```{.python .input  n=5}
%matplotlib inline
import matplotlib as mpl
mpl.rcParams['figure.dpi']= 120
import matplotlib.pyplot as plt
import numpy as np

def train(batch_size, lr, epochs, period):
    assert period >= batch_size and period % batch_size == 0
    [w, b], sqrs = init_params()
    total_loss = [np.mean(square_loss(net(X, w, b), y).asnumpy())]

    # 注意epoch从1开始计数。
    for epoch in range(1, epochs + 1):
        for batch_i, data, label in data_iter(batch_size):
            with autograd.record():
                output = net(data, w, b)
                loss = square_loss(output, label)
            loss.backward()
            adagrad([w, b], sqrs, lr, batch_size)
            if batch_i * batch_size % period == 0:
                total_loss.append(np.mean(square_loss(net(X, w, b), y).asnumpy()))
        print("Batch size %d, Learning rate %f, Epoch %d, loss %.4e" % 
              (batch_size, lr, epoch, total_loss[-1]))
    print('w:', np.reshape(w.asnumpy(), (1, -1)), 
          'b:', b.asnumpy()[0], '\n')
    x_axis = np.linspace(0, epochs, len(total_loss), endpoint=True)
    plt.semilogy(x_axis, total_loss)
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.show()
```

使用Adagrad，最终学到的参数值与真实值较接近。

```{.python .input  n=6}
train(batch_size=10, lr=0.9, epochs=3, period=10)
```

```{.json .output n=6}
[
 {
  "name": "stdout",
  "output_type": "stream",
  "text": "Batch size 10, Learning rate 0.900000, Epoch 1, loss 5.0214e-05\nBatch size 10, Learning rate 0.900000, Epoch 2, loss 4.8752e-05\nBatch size 10, Learning rate 0.900000, Epoch 3, loss 4.9236e-05\nw: [[ 2.0011935 -3.4002175]] b: 4.2014346 \n\n"
 },
 {
  "data": {
   "image/png": "iVBORw0KGgoAAAANSUhEUgAAAY4AAAEKCAYAAAAFJbKyAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMi4yLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvhp/UCwAAIABJREFUeJzt3XmUXOV55/HvU1W97+pNrW61JNQtoWYXimzMYuyAER6zOHZssOMkDjEhE7LMyckET5yZM0uOk5lzZs7JhATjmLGdEDDecmQMxhjbCGwZEGLTgqSWhHapWy2p96WWZ/7oktK0tVRLVX2rbv8+5/Sh63L73uftatWv3ve99y1zd0RERDIVCboAEREpLAoOERGZEQWHiIjMiIJDRERmRMEhIiIzouAQEZEZUXCIiMiMKDhERGRGFBwiIjIjsaALyIWGhgZfvHhx0GWIiBSUV1999ai7N55rv1AGx+LFi9mwYUPQZYiIFBQz25PJfhqqEhGRGQlVcJjZbWb2cH9/f9CliIiEVqiCw92/5+731tTUBF2KiEhohSo4REQk9xQcIiIyIwoOERGZEQWHiIjMiIJjih9sOsyX1+0KugwRkbyW98FhZheZ2VfM7Fu5Ptfz23v4koJDROSsAgkOM3vEzHrMbNO07WvMbJuZdZvZAwDuvsvd75mNuhqrSukbHieRTM3G6UREClJQPY6vAmumbjCzKPAgcCvQBdxtZl2zWVRzdQnucHRoYjZPKyJSUAIJDndfBxybtnk10J3uYUwAjwN3zGZdTVWlABwZGJvN04qIFJR8muNoBfZNebwfaDWzejN7CLjKzD5/ph82s3vNbIOZbejt7T2vApqrSwDoGRw/r58XEZkL8n51XHfvA+7LYL+HzewQcFtxcfHV53Mu9ThERM4tn3ocB4CFUx63pbdl7ELXqmqoLMZMPQ4RkbPJp+B4Beg0syVmVgzcBaydyQEudHXcWDRCfUUJvYPqcYiInElQl+M+BqwHlpvZfjO7x90TwP3AM8BW4Al33zyT42ZjddymqhKODKjHISJyJoHMcbj73WfY/hTw1Pke18xuA27r6Og430PQXF1Cj3ocIiJnlE9DVRcsGz2O+TVl7Ds2irtnsTIRkfAIVXBkw+VtNfSPxtl1dDjoUkRE8lKogiMbHx27alEdAK/uOZ6tskREQiVUwZGNoaqljZXUlBXx6jsKDhGR0wlVcGRDJGKsbK/llT3TV0QREREIWXBkY6gK4NqOBnb1DrOnT/McIiLThSo4sjFUBXDLJfMBeHbLkWyUJSISKqEKjmxZOK+cFS3VPLP5cNCliIjknVAFR7aGqgBuXN7Ia3tPMJ5IZqEyEZHwCFVwZGuoCuDSBTUkUs6OI0NZqExEJDxCFRzZdMmCagA2H7zw3ouISJgoOM6gfV45lSUxNh8cCLoUEZG8ouA4g0jEWNFSxRYFh4jIu4QqOLI5OQ7Q1VLN1kMDWvBQRGSKUAVHNifHAZbNr2J4IsmBE6NZOZ6ISBiEKjiyrbOpCoAdPbqySkTkJAXHWSxrrgRgx5HBgCsREckfCo6zqC0vprGqhO26l0NE5BQFxzksa65Uj0NEZIpQBUe2r6oCWNZcxfYjQ8STqawdU0SkkIUqOLJ9VRXAe5bUMxpP8treE1k7pohIIQtVcOTCNUvriRi8uKM36FJERPKCguMcasqKuGJhLS90Hw26FBGRvKDgyMD1HQ28se8E/aPxoEsREQmcgiMD13U2knJYv1O9DhERBUcGrmqvpaI4ygs7FBwiIgqODBRFI7z3onpe1DyHiEj+B4eZVZjZ18zsy2b26aDquK6zgT19I+w7NhJUCSIieSGQ4DCzR8ysx8w2Tdu+xsy2mVm3mT2Q3vxrwLfc/XPA7bNebNr1nQ0AGq4SkTkvqB7HV4E1UzeYWRR4ELgV6ALuNrMuoA3Yl94tOYs1vsvSxkpaakp5sVv3c4jI3BZIcLj7OuDYtM2rgW533+XuE8DjwB3AfibDA85Sr5nda2YbzGxDb2/2X9zNjOs6GvhZdx/JlD7YSUTmrnya42jl33oWMBkYrcB3gI+Z2T8A3zvTD7v7w+6+yt1XNTY25qTA6zob6B+Ns+lA9tbCEhEpNLGgCzgXdx8GPpvJvmZ2G3BbR0dHTmq5tmNynuPF7qNcsbA2J+cQEcl3+dTjOAAsnPK4Lb0tY7lY5HCqhsoSulqqeUHrVonIHJZPwfEK0GlmS8ysGLgLWDuTA+RiWfXpru9s4NU9xxkeT+TsHCIi+Syoy3EfA9YDy81sv5nd4+4J4H7gGWAr8IS7b57JcXPd4wB4/7JG4knn5zv7cnYOEZF8Fsgch7vffYbtTwFPne9xcz3HAbBq8TwqiqP8dFsPN3c15+w8IiL5Kp+Gqi7YbPQ4imMR3tfRwE+39eKuy3JFZO4JVXDMxhwHwI3LGzlwYpSdvUM5PY+ISD4KVXDMRo8D4MblTQD8dJuurhKRuSdUwTFbWmvL6GyqVHCIyJwUquCYraEqgA9c3MTLu48xOKZPBRSRuSVUwTFbQ1UAt1wyn4lkime3HMn5uURE8kmogmM2rWyvpbW2jLVvHAy6FBGRWaXgOE9mxm1XLODFHUc5NjwRdDkiIrMmVMExm3McALdfsYBEynnqrUOzcj4RkXwQquCYzTkOgBUtVSxtrOB7Gq4SkTkkVMEx28yM269o5eV3jrGnbzjockREZoWC4wLdtXohRZEI//jC7qBLERGZFaEKjtme4wBori7lzqsW8M1X9zGgezpEZA4IVXDM9hzHSZ9YtZCxeIoXth+d1fOKiAQhVMERlKva66gtL+K5t3UzoIiEn4IjC6IR48Zljfx0Wy/JlJZaF5FwU3BkyfuXN3JseIK3Dw8EXYqISE4pOLJk1aJ5AGzcczzgSkREckvBkSVtdWU0VpWwce+JoEsREcmpUAVHEJfjTjk3V7fX8ap6HCIScqEKjqAuxz3p6kV17D02wuH+sUDOLyIyG0IVHEH7wMWNAPxwy+GAKxERyR0FRxZ1NFXR0VTJ028pOEQkvBQcWXbrpfN5aXcffUPjQZciIpITCo4sW3PpfFIOP9RHyopISCk4sqyrpZr2eeU8vUnDVSISTnkfHGZ2kZl9xcy+FXQtmTAzbr1sPj/vPkr/iFbLFZHwyWlwmNkjZtZjZpumbV9jZtvMrNvMHjjbMdx9l7vfk8s6s+3WS1tIpJwfbdVwlYiET657HF8F1kzdYGZR4EHgVqALuNvMuszsMjN7ctpXU47ry4kr2mpYUFOq4SoRCaVYLg/u7uvMbPG0zauBbnffBWBmjwN3uPsXgY/ksp7ZYmbccul8Hn1pL0PjCSpLcvprFhGZVUHMcbQC+6Y83p/edlpmVm9mDwFXmdnnz7LfvWa2wcw29Pb2Zq/a8/TBi5uYSKR4ba+WIBGRcMn7yXF373P3+9x9abpXcqb9Hnb3Ve6+qrGxcTZLPK3L22oBeF2LHopIyAQRHAeAhVMet6W3XbAgFzmcrqasiKWNFbyxX8EhIuESRHC8AnSa2RIzKwbuAtYGUEfOXbmwjtf3ncBdnwooIuGR68txHwPWA8vNbL+Z3ePuCeB+4BlgK/CEu2/OxvmCXh13uisX1nB0aIL9x0eDLkVEJGtyfVXV3WfY/hTwVC7PnQ+uWVoPwPPbe/mN9y4KuBoRkezI+8nxmcinOQ6ApY2VLJxXxk/e7gm6FBGRrAlVcOTbUJWZ8cHlTfxs51HG4smgyxERyYpQBUe+9TgAPnBxE2PxFL/Y1Rd0KSIiWRGq4Mi3HgfAey+qp7QoouEqEQmNUAVHPiotinLt0gZ+vK1Hl+WKSCiEKjjycagKJoer9h0bpbtnKOhSREQuWKiCIx+HqgA+1NWMGXz/rUNBlyIicsFCFRz5qqm6lNWL5/Hkm4c0XCUiBS9UwZGvQ1UAH7liAd09QxquEpGCl1FwmNkfm1m1TfqKmW00sw/luriZytehKoAbl02u2Ltel+WKSIHLtMfxO+4+AHwIqAM+A/x1zqoKoba6MhbUlPLS7mNBlyIickEyDQ5L//fDwD+lFyW0s+wv05gZq5fM4+XdxzTPISIFLdPgeNXMfshkcDxjZlVAKndlhdPqJfX0Do6z++hw0KWIiJy3TIPjHuAB4FfcfQQoAj6bs6rOUz5PjgNc19EAwE+2Bf/RtiIi5yvT4LgG2ObuJ8zsN4AvAHn36pzPk+MA7fXlLGuu5EdbjgRdiojIecs0OP4BGDGzK4A/BXYCX89ZVSF204pmXn7nGP0j8aBLERE5L5kGR8InZ3TvAP7O3R8EqnJXVnjd1NVMMuX8dLsWPRSRwpRpcAya2eeZvAz3+2YWYXKeQ2boyrZaGipL+KGGq0SkQGUaHJ8Expm8n+Mw0Ab8r5xVFWKRiHHTiiae39bLREIXpolI4ckoONJh8ShQY2YfAcbcPe/mOPL9qqqTblrRzNB4gpd26y5yESk8mS458gngZeDXgU8AL5nZx3NZ2PnI96uqTrqus4HSooiurhKRgpTpUNVfMHkPx2+5+28Cq4G/zF1Z4VZaFOX6zkae3XJEd5GLSMHJNDgi7j71MqC+GfysnMbNK5o52D/G5oMDQZciIjIjsQz3+4GZPQM8ln78SeCp3JQ0N3xwRRNm8NzWHi5tze+hNRGRqTKdHP8z4GHg8vTXw+7+57ksLOwaKktY2V7Hs1sPB12KiMiMZNrjwN2/DXw7h7XMOTetaOZvfvA2h/pHaakpC7ocEZGMnLXHYWaDZjZwmq9BM9Pg/AW6uasZQFdXiUhBOWtwuHuVu1ef5qvK3atnq0gzu9PMvmxm38jHTx48X0sbK1jSUMGzW7X8iIgUjpxfGWVmj5hZj5ltmrZ9jZltM7NuM3vgbMdw9391988B9zE5MR8KZpN3ka/feZTBMS16KCKFYTYuqf0qsGbqBjOLAg8CtwJdwN1m1mVml5nZk9O+mqb86BfSPxcaN3fNJ5501m0/GnQpIiIZyXhy/Hy5+zozWzxt82qg2913AZjZ48Ad7v5F4CPTj2FmxuRnnD/t7htPdx4zuxe4F6C9vT1r9efayvZa6sqL+NHWI/y7y1uCLkdE5JyCuomvFdg35fH+9LYz+UPgJuDjZnbf6XZw94fdfZW7r2psbMxepTkWi0b4wMVN/PjtHuJJLXooIvmvIO7+dve/dfer3f0+d3/oTPsVyiKH032oq5n+0Tgb3jkedCkiIucUVHAcABZOedyW3nZBCmWRw+mu72ykOBrhWV2WKyIFIKjgeAXoNLMlZlYM3AWsvdCDFmqPo6Ikxvs66nl262EteigieW82Lsd9DFgPLDez/WZ2j7sngPuBZ4CtwBPuvvlCz1WoPQ6YvBlw37FRth8ZCroUEZGzynlwuPvd7t7i7kXu3ubuX0lvf8rdl7n7Unf/q2ycq1B7HDC5Wq4ZfP/Ng0GXIiJyVgUxOZ6pQu5xNFWXcu3SBr77+gENV4lIXgtVcBRyjwPgzqta2XdslI17dXWViOSvUAVHIfc4AG65pJlYxHhOa1eJSB4LVXAUuqrSIlYuquP57b1BlyIickahCo5CH6oCeP+yRjYfHKBncCzoUkRETitUwVHoQ1UwGRwAL2jRQxHJU6EKjjDoaqmmobKEdTs0XCUi+UnBkWciEeOGZQ2s295LMqXLckUk/4QqOMIwxwGTw1XHR+K8daCw2yEi4RSq4AjDHAfADZ2NRAye26pFD0Uk/4QqOMKirqKY9yyp5+lNh4MuRUTklyg48tStl82nu2eI7p7BoEsREXmXUAVHWOY4AG65ZD4AT7+lXoeI5JdQBUdY5jgAmqtLuXpRnYarRCTvhCo4wmbNJfPZcmiAPX3DQZciInKKgiOPrbl0Pmbw2Mv7gi5FROQUBUceWzivnNuvWMDXfv6O1q4Skbyh4Mhz/+GmZUwkU/z9T3YGXYqICKDgyHuLGyr4xKqFPPrSHvYdGwm6HBGRcAVHmC7HneoPP9hBPOmsfUOfRy4iwQtVcITpctypFtSW0VZXxtZDA0GXIiISruAIsxUt1QoOEckLCo4CsaKlmt1HhxmLJ4MuRUTmOAVHgehqqSLlsO2w1q4SkWApOArEipZqALZouEpEAqbgKBDt88ppqCzmpV19QZciInNc3geHma0ws4fM7Ftm9vtB1xMUM+N9Sxt4sbsPd32krIgEJ6fBYWaPmFmPmW2atn2NmW0zs24ze+Bsx3D3re5+H/AJ4Npc1pvvruto4OjQONuPDAVdiojMYbnucXwVWDN1g5lFgQeBW4Eu4G4z6zKzy8zsyWlfTemfuR34PvBUjuvNa9d2NgDwYvfRgCsRkbksp8Hh7uuAY9M2rwa63X2Xu08AjwN3uPtb7v6RaV896eOsdfdbgU/nst5811pbxkUNFfxMwSEiAYoFcM5WYOo64fuB95xpZzO7Efg1oISz9DjM7F7gXoD29vZs1JmXru1o4Nsb9xNPpiiK5v0UlYiEUN6/8rj7T939j9z999z9wbPs97C7r3L3VY2NjbNZ4qy6tqOBkYkkr+09EXQpIjJHBREcB4CFUx63pbddsLAucjjVNUvriUaMH7/dE3QpIjJHBREcrwCdZrbEzIqBu4C1AdRRkGrKiri+s4G1rx8gldJluSIy+3J9Oe5jwHpguZntN7N73D0B3A88A2wFnnD3zdk4X1hXx53uo1e1crB/jJd2T7/uQEQk93I6Oe7ud59h+1PM8UtrL8SHuuZTVRLjn1/awzVL64MuR0TmmLyfHJ+JuTDHAVBWHOVT72nn6bcOsbdPnwooIrMrVMExV4aqAD577RIiZjz+yt6gSxGROSZUwTFXehwA82tKWdlex7odvUGXIiJzTKiCYy71OABuWNbApgMD9A2NB12KiMwhoQqOueb6zskbHbV2lYjMplAFx1waqgK4tLWG2vIi1m1XcIjI7AlVcMy1oapoxLi2o4EXdvTqMzpEZNaEKjjmovd3NtIzqM/oEJHZE6rgmGtDVQDXpT+j4/ntWrtKRGZHqIJjrg1VASyoLaOrpZqn3jocdCkiMkeEKjjmqtuvXMDr+07oLnIRmRUKjhC47YoFAHzvzYMBVyIic4GCIwRaa8u4rLWG57fpLnIRyb1QBcdcnBw/6frOBjbuPc7QeCLoUkQk5EIVHHNxcvyk6zsbSaScX+zsC7oUEQm5UAXHXHb1ojrKi6P8aOuRoEsRkZBTcIREcSzChy9r4XtvHGRYw1UikkMKjhC5e3U7wxNJ1r6hq6tEJHcUHCGysr2WzqZKvv3q/qBLEZEQC1VwzOWrqgDMjI+ubGXDnuPs6RsOuhwRCalQBcdcvqrqpDuvbMUMvvvagaBLEZGQClVwyOTaVddcVM93XzugpdZFJCcUHCH00ata2dM3wsa9J4IuRURCSMERQrde1kJpUYRvbtgXdCkiEkIKjhCqLIlx55WtfPe1Axwbngi6HBEJGQVHSP3OdUsYT6T40rqdQZciIiGj4AipZc1VfGxlG196fpfu6xCRrCqI4DCzCjPbYGYfCbqWQvLXH7uMrpZqvrb+naBLEZEQyWlwmNkjZtZjZpumbV9jZtvMrNvMHsjgUH8OPJGbKsOrKBrhlkvm89aBfs11iEjW5LrH8VVgzdQNZhYFHgRuBbqAu82sy8wuM7Mnp301mdnNwBagJ8e1htINyxpwhxd26EOeRCQ7Yrk8uLuvM7PF0zavBrrdfReAmT0O3OHuXwR+aSjKzG4EKpgMmVEze8rdU6fZ717gXoD29vYstqKwXd5WS215ET/YdJg7rmwNuhwRCYEg5jhagak3GOxPbzstd/8Ld/8T4F+AL58uNNL7Pezuq9x9VWNjY1YLLmTRiPGp1e08vekwmw7MzTW8RCS7CmJyHMDdv+ruT55tn7m+yOGZ/N77l1JbXsTf/ODtoEsRkRAIIjgOAAunPG5Lb7tgWuTw9GrKirj/Ax28sOOo5jpE5IIFERyvAJ1mtsTMioG7gLXZOLB6HGf2mWsWsXBeGZ/7+gbueng9/+PJLQzpkwJF5Dzk+nLcx4D1wHIz229m97h7ArgfeAbYCjzh7puzcT71OM6sJBblG/dew62XtjA6keQrP9vNp7/8C8YTyaBLE5ECY2FaetvMbgNu6+jo+NyOHTuCLievPf3WIX7/0Y186j3t/NWdl2JmQZckIgEzs1fdfdW59iuYyfFMqMeRuVsva+G+9y/lX17ay39/civHdYOgiGQop/dxzLYpPY6gSykI//GW5fSPTvDIz3bzvTcP8qnV7bTUlPKxq9soiobqPYWIZFGohqpOWrVqlW/YsCHoMgrGpgP9/MG/bGRP3wgA1aUxljZV8iuL5/G71y2hqbo04Arzz+v7TnC4f5Q1l7bM+rn7R+J8Y8Neyotj3NDZSHt9+Yx+fseRQQbG4lyyoIbSoihj8SSv7jnOgtoy5leXUloUKZihS3end3CcxqqSgqk5n2U6VKXgEAASyRQj8SSv7D7Gc2/3sLNniA17jhOLGLdcMp9oxDg6NE5DZQkdTZV0tVTTOzhOVWmMwbEE1yytZ+G8yRewnoExvvLibt460M///PjltNW9+4XN3RlPpCiJvfsFKpFMMRpPUlkSw8wYnUjyg82HGBhN0NlUSXlJjD19wyxrrqIoGuG5rUdoqi7h+HCci1uq2HJwgMvbalm9ZB7uzo6eISpLYlSUxPjh5sPs6RvhkgXVjEwkOT4yQXlxjNryIuZVFFMUjVBfUczCeeVEI3aqzp29QxwZGKd/NM6JkTi7eoc4NDDGs1uOMJFI8We3LOe6jgYub6vh+EicPX3DLKqv4PjIBG/sO8HCeeUsn19FcTRCaVH0rM9BPJli88EBdvYMUVES47W9x7mpq5n2eeVsOTjAWDzJ3/64m/3HRhicckXcZa01rLl0PkVRY2A0warFdSyfX8VYPMXQWIJFDeWUxCJsPTTIIy/uZu0bBwFoqirhzqsmP7eld3D81PGWN1fxxfQCmWPxJNGIETFj88EBEskU//D8TuLJFH/x4S6+vv4dRuNJ7v9gB3XlxdSUFf1SO5MpZ3giQVX6eQUYiycZj6eoKo0Ribz7BX/fsRHW7+yjsbqE/cdHmUikWNFSxfLmKl7sPsrX1+9hWXMVHU2VfGfjfjYfHODythpKYhE+9Z52br+ilfFEktJY9NSxewbHeOinu6ivLMYMblzWRNeC6lPnHByLs+3wIK11ZTRVlfLznUd55Z3jbDk4QHN1CTcsa6S6tIhdR4doqyvnhs4GeofGiSedqBnDEwnKi6OUFUUpLYq+6287lXK29wwyOJagtbaMnb1DjE4kubytlubqElIOEYOTL8XHRiY4cHyUn+/s46XdfeztG+FzN1zEyvY6XtjRy8BYgvZ55Xz4svkcGRhnT98wXQuqqa8oYd+xEebXlJ7zb+1M5mRwaHI8u/b0DfP3P9nJuh29pNyZX13K0aEJDvaPcro/m9ryIlIpZzSeJJlySmJRYhGjvrKY4YkkYxNJku7Ek6nJf3ARo6o0RnlRlMGxxKkXw+JohPrKYobGEwyOzfyS4eJYhIjBWPy0iwycU2lRhMqSGCMTSUYm3n3VWXE0QlN1CYvrK0i58/OdfcDkvTLD4wkSqTP/e6otLyKRdEqLogyPJ6gui+EOKff0i2uSicTZa76osYJVi+r4zWsWU1Ua45nNh/n+m4d4Y39ml6CXF0f57LWLuay1lq+vf4f1u/pY3lzFn35oOTt6BhkeT/DNDfvpGRzHpryYTRVLvxgnUk5xLIIB41PqLolFTrWpKBohYsZoPElVSYzqsiJOjEwwnP69RiNGZUkMd8eZPN/IRIKz/BpZXF/O0aEJhsYn31Dc1NXMT97uYSKRYtfRYYpjESYSKSLGqfPHk6lTNcPkC3VjVQklsSjxZIpD/WPval8i5UQMljRUcPDEGKPxd/8dVBRHT7XhdCIGZekX75F48rS/x5PnSrpTEosQTzpFUXvX3237vHJqyop46zSrPkSMd/2eTj5f37rvGlYtnnfmX+BZzMngOEk9jtza2zfC3mMjLKgtZWAsQUVxlBe7j7KjZ4jiaOTUO7/h8ST/72e7GU+kqCiZfCcWixjRSITKkigjE0kGxxKTLyqlMWrLiikrjnBsOM7RoXGiZnx0ZSsXNVSw7cggY/EUC2pLefvQIPFkims7GhgcS1BVGuOVd47R2VTFxr3HOTwwRjyRYllzFeOJJEPjSVYtruOqhbW80H2UmrIiOpsqGR5PcmJ0gr6hCeLJFIf7xzg8MMbIRJKh8QQlsQgXz69iUX0FlSUxGipLqCqd7MHA5DvpXb1DvLbvBG/uP0FVaRFXLqxl//FR3J3rOxvZe2yEXb1DjMVT9AyOURyLMDox2asaGIsTMSMSMaJmlBVHuaKtls7mSnoHx+lqqWbdjl56B8e5eH41x0cm+NUVTZQX//LUZN/QOMWxCMWxCC/uOMqRgXFKiyKUF8fo7hkk5bC4oYIbOhuoLS8+9XPD4wnKiqLvetc/NJ7gmxv2cWIkTk1ZESl34kmnPd0ba6sro390svd1yyXzGU+keOWdY4zGkxwfnmBgLEE0YsQixlg8STIFTdUlHDoxysBYgrryYuoriymJRTgxEmdwLI6ZYQaGUV0W4+auZgbHEiyqL6coGmHTgX66e4ZorS3j5q5mHDhwfJRF9eWn3tknU87Tmw7x2t4TzKsoZnQiSTyVwh0iZnxsZSv1lSUkkikefWkvh/vHGE8kiUYiLK4vZ0VLNe/0DdM3PMFlrTXcuLyR8uIYY/EkWw4NMDaRpLWujFfeOc6mA/00V5dSWRoDd6rLihhNv9EYjScZi09+7w6VJVHa6sqpLiuid2icjsZKimMR3tx/gsMDY5REI4zGkxRFI4wnUiysK6OuopjrOxuZV1GMu/OLXcfYdniAWy6dT2NlCRv3nuDZLYdZ2lhJ+7xyth4e5PjwBO3zyrnx4kaaqs5veFnBoeAQEZmROXk5roiI5F6ogkNLjoiI5F6ogkM3AIqI5F6ogkNERHJPwSEiIjMSquDQHIeISO6FKjg0xyEiknuhCg4REcm9UN4AaGa9wJ7z/PEG4GgWywmS2pJ/wtIOUFvy0YW2Y5G7N55rp1AGx4Uwsw2Z3DkW6b6UAAAFfklEQVRZCNSW/BOWdoDako9mqx0aqhIRkRlRcIiIyIwoOH7Zw0EXkEVqS/4JSztAbclHs9IOzXGIiMiMqMchIiIzMmeDw8zWmNk2M+s2swdO8/9LzOwb6f//kpktnv0qM5NBW37bzHrN7PX01+8GUee5mNkjZtZjZpvO8P/NzP423c43zWzlbNeYiQzacaOZ9U95Pv7zbNeYKTNbaGY/MbMtZrbZzP74NPsUyvOSSVvy/rkxs1Ize9nM3ki347+eZp/cvn65+5z7AqLATuAioBh4A+iats+/Bx5Kf38X8I2g676Atvw28HdB15pBW24AVgKbzvD/Pww8DRjwXuCloGs+z3bcCDwZdJ0ZtqUFWJn+vgrYfpq/r0J5XjJpS94/N+nfc2X6+yLgJeC90/bJ6evXXO1xrAa63X2Xu08AjwN3TNvnDuBr6e+/BfyqnfyMyvySSVsKgruvA46dZZc7gK/7pF8AtWbWMjvVZS6DdhQMdz/k7hvT3w8CW4HWabsVyvOSSVvyXvr3PJR+WJT+mj5ZndPXr7kaHK3AvimP9/PLf0Cn9nH3BNAP1M9KdTOTSVsAPpYeRviWmS2cndKyLtO2FoJr0kMNT5vZJUEXk4n0cMdVTL7DnargnpeztAUK4Lkxs6iZvQ70AM+6+xmfk1y8fs3V4JhrvgcsdvfLgWf5t3ciEoyNTC7tcAXwf4F/DbieczKzSuDbwJ+4+0DQ9VyIc7SlIJ4bd0+6+5VAG7DazC6dzfPP1eA4AEx9192W3nbafcwsBtQAfbNS3cycsy3u3ufu4+mH/whcPUu1ZVsmz1vec/eBk0MN7v4UUGRmDQGXdUZmVsTkC+2j7v6d0+xSMM/LudpSaM+Nu58AfgKsmfa/cvr6NVeD4xWg08yWmFkxk5NHa6ftsxb4rfT3Hwd+7OmZpjxzzrZMG2++ncmx3UK0FvjN9FU87wX63f1Q0EXNlJnNPznebGarmfx3mI9vSkjX+RVgq7v/7zPsVhDPSyZtKYTnxswazaw2/X0ZcDPw9rTdcvr6FcvWgQqJuyfM7H7gGSavSnrE3Teb2X8DNrj7Wib/wP7JzLqZnOi8K7iKzyzDtvyRmd0OJJhsy28HVvBZmNljTF7V0mBm+4H/wuTEH+7+EPAUk1fwdAMjwGeDqfTsMmjHx4HfN7MEMArcladvSgCuBT4DvJUeUwf4T0A7FNbzQmZtKYTnpgX4mplFmQy2J9z9ydl8/dKd4yIiMiNzdahKRETOk4JDRERmRMEhIiIzouAQEZEZUXCIiMiMKDhE8kx6hdYng65D5EwUHCIiMiMKDpHzZGa/kf5chNfN7EvpheeGzOz/pD8n4Tkza0zve6WZ/SK90OR3zawuvb3DzH6UXlRvo5ktTR++Mr0g5dtm9mierswsc5SCQ+Q8mNkK4JPAtenF5pLAp4EKJu/evQR4nsm7xgG+Dvx5eqHJt6ZsfxR4ML2o3vuAk0t1XAX8CdDF5GetXJvzRolkaE4uOSKSBb/K5GKRr6Q7A2VMLnGdAr6R3uefge+YWQ1Q6+7Pp7d/DfimmVUBre7+XQB3HwNIH+9ld9+ffvw6sBh4MffNEjk3BYfI+THga+7++XdtNPvLafud75o+41O+T6J/q5JHNFQlcn6eAz5uZk0AZjbPzBYx+W/q4+l9PgW86O79wHEzuz69/TPA8+lPodtvZnemj1FiZuWz2gqR86B3MSLnwd23mNkXgB+aWQSIA38ADDP5wTpfYHLo6pPpH/kt4KF0MOzi31aQ/QzwpfTKpnHg12exGSLnRavjimSRmQ25e2XQdYjkkoaqRERkRtTjEBGRGVGPQ0REZkTBISIiM6LgEBGRGVFwiIjIjCg4RERkRhQcIiIyI/8fCVIydjcA4xoAAAAASUVORK5CYII=\n",
   "text/plain": "<Figure size 432x288 with 1 Axes>"
  },
  "metadata": {},
  "output_type": "display_data"
 }
]
```

## 结论

* **Adagrad是一个在迭代过程中不断自我调整学习率，并让模型参数中每个元素都使用不同学习率的优化算法。**


## 练习

* 我们提到了Adagrad可能的问题在于按元素平方的梯度累加变量。你能想到什么办法来应对这个问题吗？

**吐槽和讨论欢迎点**[这里](https://discuss.gluon.ai/t/topic/2273)
